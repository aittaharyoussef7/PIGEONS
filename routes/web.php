<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/**********************************Route Admin****************************************-************/
Route::get('/downloadfactureBDPDF/{id}', 'Admin\FactureController@downloadfactureBDPDF'); /*telecharger Facture de bids */
Route::get('/downloadfacturePDF/{id}', 'Admin\FactureController@downloadfacturePDF'); /*telecharger Facture de Pigeons */
Route::get('/recuperercmd/', 'Admin\CmdController@recuperercmd');/*La list des cmd suprimer */
Route::get('/recuperercmdid/{id}/{pre_id}', 'Admin\CmdController@recuperercmdid');/*Pour récupéer une cmd par id  */
Route::get('/deletcmd/{id}/{pre_id}', 'Admin\CmdController@deletcmd');/*supprimer ine cmd */
Route::get('/valider/{id}/{pre_id}', 'Admin\CmdController@valider');/*Valider une cmd */
Route::get('/authBD/{id}/{pre_id}', 'Admin\AutorisationController@authBD');/*Authorisation Annonces bids  */
Route::get('/authPG/{id}/{pre_id}', 'Admin\AutorisationController@authPG');/*Authorisation Annonces pigeons */
Route::get('/authPR/{id}/{pre_id}', 'Admin\AutorisationController@authPR');
Route::get('/welcomePG/{id}/{pre_id}', 'Admin\welcomeController@welcomePG');/*Authorisation Annonces  produit */
Route::get('/welcomePR/{id}/{pre_id}', 'Admin\welcomeController@welcomePR');
Route::get('/welcomeBD/{id}/{pre_id}', 'Admin\welcomeController@welcomeBD');
Route::get('/Storecat/{nom}', 'Admin\HomeAdminController@Storecat');


Route::post('/store_contact', 'Admin\SettingController@store_contact');
Route::get('/setting', 'Admin\AutorisationController@setting');
Route::get('/updateinfo/{email}/{tel}/{adresse}/{tva}', 'Admin\SettingController@updateinfo');
Route::get('/colorN/{colorN}', 'Admin\SettingController@colorN');
Route::get('/colorM/{colorM}', 'Admin\SettingController@colorM');
 /*Créer une catégorie  */
Route::get('/Add_admin', 'Admin\SettingController@Add_admin'); /*Ajouter une Admin  */
Route::post('/RechercherRecupCmd', 'Admin\CmdController@RechercherRecupCmd'); /*Rechercher a une cmd supprimer par id */
Route::post('/RechercherCmd', 'Admin\CmdController@RechercherCmd'); /*Rechercher a une cmd  par id */

Route::get('/deletecat/{id}', 'Admin\HomeAdminController@deletecat'); /*Supprimer une catégorie  */

Route::get('/CmdPg', 'Admin\CmdController@listcmdpig');  /*La list des cmd de pigeons et Produit  */
Route::get('/authclient/{id}', 'Admin\AutorisationController@authclient');  /*La list des cmd de pigeons et Produit  */



Route::get('/add_panierPG/{id}', 'PanierController@add_panierPG');
Route::get('/add_panierPR/{id}', 'PanierController@add_panierPR');
Route::get('/remove_panierPG/{id}', 'PanierController@remove_panierPG');


Route::get('/remove_panierPR/{id}', 'PanierController@remove_panierPR');
Route::get('/remove_panierPG2/{id}', 'PanierController@remove_panierPG2');
Route::get('/remove_panierPR2/{id}', 'PanierController@remove_panierPR2');

Route::get('/Allpanier', function () {           //view Acceuil de Cleint
    return view('cleint.panier.Allpanier');
});
/*****Autorisation Annonces ***/
Route::get('/ModifierLogin','Admin\SettingController@ModifierLogin'); 
Route::POST('/MOdifierLOGIN','Admin\SettingController@MOdifierLOGIN2'); 


Route::get('/PR','Admin\AutorisationController@PR'); 
Route::get('/NPR','Admin\AutorisationController@NPR'); 

Route::get('/APR','Admin\AutorisationController@APR'); 
Route::get('/APG','Admin\AutorisationController@APG'); 
Route::get('/NPG','Admin\AutorisationController@NPG'); 
Route::get('/infobids/{id}','Admin\AutorisationController@infobids'); 

Route::get('/bids','AnnoncesController@bids'); 
Route::get('/Nbids','Admin\AutorisationController@Nbids'); 
Route::get('/Abids','Admin\AutorisationController@Abids'); 

/***************/

Route::get('/Add_AdminMail/{email}', 'Admin\SettingController@Add_AdminMail'); /*Ajouter une Admin **/
Route::get('/deleteAdmin/{id}', 'Admin\SettingController@deleteAdmin');  /*suprimer une Admin **/

Route::get('/admin','Admin\HomeAdminController@home'); /**** home Admin ***/


Route::get('/categorie', 'Admin\HomeAdminController@categorie'); /*** la list des catégorie***/

/********************************** End Route Admin****************************************-************/

/*----------------------------------------------CreateEditAnnonces Client --------------------------------------*/


Route::get('/deletePR/{id}/{pre_id}', 'AnnoncesController@deletePR');
Route::get('/MESPG', 'AnnoncesController@MESPG');
Route::get('/MESPR', 'AnnoncesController@MESPR');
Route::get('/MESBD', 'AnnoncesController@MESBD');
Route::get('/deleteBD/{id}/{pre_id}', 'AnnoncesController@deleteBD');
Route::get('/deleteBDC/{id}/{pre_id}', 'AnnoncesController@deleteBDC');

Route::get('/CreatePR', 'AnnoncesController@CreatePR');
Route::get('/CreatePG', 'AnnoncesController@CreatePG');
Route::get('/CreateBD', 'AnnoncesController@CreateBD');

Route::get('/EditBD/{id}', 'AnnoncesController@EditBD');
Route::get('/EditPG/{id}', 'AnnoncesController@EditPG'); 

Route::get('/EditPR/{id}', 'AnnoncesController@EditPR'); 


/***************************************** Admin Crete ----------------------------*/


/***************************************** End  Admin Crete ----------------------------*/






Route::get('/deletePG/{id}/{pre_id}', 'AnnoncesController@deletePG'); 
Route::get('/deletePGC/{id}/{pre_id}', 'AnnoncesController@deletePGC'); 
Route::get('/deletePRC/{id}/{pre_id}', 'AnnoncesController@deletePRC'); 

Route::post('/UpdatePR/{id}', 'AnnoncesController@UpdatePR'); 
Route::post('/UpdateBD/{id}', 'AnnoncesController@UpdateBD');

Route::post('/UpdatePG/{id}', 'AnnoncesController@UpdatePG'); 
Route::post('/StoreBD', 'AnnoncesController@StoreBD'); 
Route::post('/StorePG', 'AnnoncesController@StorePG'); 
Route::post('/StorePR', 'AnnoncesController@StorePR'); 

/*-----------------------------End Route Create Edit Annonces --------------------------------------*/



/* --------------------------------------Route Cleint ---------------------------------*/

Route::get('/logins', function () {           //view Acceuil de Cleint
    return view('auth.login');
});



Route::get('/', function () {           //view Acceuil de Cleint
    return view('cleint.welcome.welcome');
});
Route::get('/contact', function () {           //view Contact de Cleint
    return view('cleint.contact');
});


Route::get('/login', function () {     //view login  de Cleint
    return view('cleint.login');
});

Route::get('/FiltreBDDATE/{date}', 'Client\ConsulterAnnoncesController@FiltreBDDATE'); 
Route::get('/FiltreBD', 'Client\ConsulterAnnoncesController@FiltreBD'); 
Route::get('/FiltrePRIXPR', 'Client\ConsulterAnnoncesController@FiltrePRIXPR'); 
Route::get('/FiltrePRIXPG', 'Client\ConsulterAnnoncesController@FiltrePRIXPG'); 
Route::get('/FiltreCATPG', 'Client\ConsulterAnnoncesController@FiltreCAT'); 

Route::get('/FiltreNom/{nom}', 'Client\ConsulterAnnoncesController@FiltreNom'); 

Route::get('/bids', 'Client\ConsulterAnnoncesController@bids');
Route::get('/produits', 'Client\ConsulterAnnoncesController@produits'); 
Route::get('/pigeons', 'Client\ConsulterAnnoncesController@pigeons');  //all list pigeons

Route::get('/recherech/{nom}', 'Client\ConsulterAnnoncesController@recherech');
Route::get('/recherechAdmin/{nom}', 'Admin\HomeAdminController@recherech');


Route::get('/TousRecherche/{nom}', 'Client\ConsulterAnnoncesController@TousRecherche');

Route::get('/bidsPG', function () {       //all list bid 1 seul
    return view('cleint.bidsPG');
});

Route::get('/participerbids', function () {     //la list des participant des pigeons et votre detail
    return view('cleint.participerbids');
});


Route::post('/StorePar', 'Client\PasserCommandeController@StorePar');
Route::get('/ConfirmerPar/{id_par}/{checksum}', 'Client\PasserCommandeController@ConfirmerPar');
Route::get('/ConfirmerCmd/{id_cmd}/{checksum}', 'Client\PasserCommandeController@confirmercmd');
Route::post('/StoreCmd', 'Client\PasserCommandeController@StoreCmd');
Route::get('/infoBDPG/{id}', 'Client\InfoAnnoncesController@infoBDPG'); //la lsit des PG à un BIDS
Route::get('/infoPG/{id}', 'Client\InfoAnnoncesController@infoPG'); //plus info sur un pg
Route::get('/infoPR/{id}', 'Client\InfoAnnoncesController@infoPR');//plus info sur un pr
Route::get('/infoBD/{id}', 'Client\InfoAnnoncesController@infoBD');
Route::get('/infobidspg/{id}', 'Client\InfoAnnoncesController@infobidspg'); // plus informer sur un pg à un bids pour participer

Route::get('/MescmdRev', 'Client\MesCommandeController@MescmdRev'); //mes commande Recevpoire
Route::get('/deletcmdC/{id}/{pre_id}', 'Client\MesCommandeController@deletcmd');/*supprimer mes cmd  Client*/
Route::get('/deletcmdCENV/{id}/{pre_id}', 'Client\MesCommandeController@deletcmdCENV');/*supprimer mes cmd */ 


Route::get('/validerC/{id}/{pre_id}', 'Client\MesCommandeController@valider');/*Valider mes cmd  Cleint*/

Route::get('/AnnulerC/{id}/{pre_id}', 'Client\MesCommandeController@AnnulerC');/*Valider mes cmd  Cleint*/




Route::get('/Cbids', function () {     //all list bids
    return view('cleint.bids');
});




Route::get('/shoping', function () {     //shoping pour passer des commande
    return view('cleint.shoping');
});




Route::get('/livraison', function () {     //view  livrasion 
    return view('cleint.livraison');
});
Route::get('/Mescmd', 'Client\MesCommandeController@Mescmd');





/********************************** End Route Cleint****************************************-************/
































Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



