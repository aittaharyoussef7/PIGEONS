-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 03 mai 2018 à 13:37
-- Version du serveur :  10.1.31-MariaDB
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pigeons`
--

-- --------------------------------------------------------

--
-- Structure de la table `adressels`
--

CREATE TABLE `adressels` (
  `id_lev` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `id_cmd` int(200) DEFAULT NULL,
  `id_user` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `qnt` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `Rue` varchar(20) NOT NULL,
  `ville` varchar(20) NOT NULL,
  `code_postal` varchar(20) NOT NULL,
  `no` varchar(20) NOT NULL,
  `tel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adressels`
--

INSERT INTO `adressels` (`id_lev`, `created_at`, `updated_at`, `deleted_at`, `id_cmd`, `id_user`, `qnt`, `nom`, `prenom`, `Rue`, `ville`, `code_postal`, `no`, `tel`) VALUES
(1, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 1, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(2, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 2, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(3, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 3, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(4, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 4, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(5, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 9, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(6, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 10, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(7, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 11, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489'),
(8, '2018-05-03 11:31:44', '2018-05-03 11:31:44', NULL, 12, 6, 1, 'youssef', 'KHAy', 'ERz', 'méknes', '50000', '12', '0634179489');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adressels`
--
ALTER TABLE `adressels`
  ADD PRIMARY KEY (`id_lev`),
  ADD KEY `FK_PersonOrdersss` (`id_cmd`),
  ADD KEY `id_lev` (`id_lev`),
  ADD KEY `fk1sss` (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adressels`
--
ALTER TABLE `adressels`
  MODIFY `id_lev` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adressels`
--
ALTER TABLE `adressels`
  ADD CONSTRAINT `FK_PersonOrdersss` FOREIGN KEY (`id_cmd`) REFERENCES `commandes` (`id_cmd`),
  ADD CONSTRAINT `fk1sss` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
