<?php

namespace App\Http\Controllers\Client;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;
use App\Http\Controllers\Controller;

class InfoAnnoncesController extends Controller
{



public function infoPG($id){

   $pigs = DB::table('pigeons')->where('deleted_at',null)->where('id_pig',$id)->get();

return view('cleint.infoAnnonces.infoPG',compact('pigs'));

    }




public function infoPR($id){

   $pros = DB::table('produits')->where('deleted_at',null)->where('id_pro',$id)->get();

return view('cleint.infoAnnonces.infoPR',compact('pros'));

    }

public function infoBD($id){

   $bids = DB::table('bids')->where('deleted_at',null)->where('id_bid',$id)->get();

return view('cleint.infoAnnonces.infoBD',compact('bids'));

    }


    public function infoBDPG($id){

   $pgs = DB::table('pigeons')->join('bids_pigs','bids_pigs.id_pig','=','pigeons.id_pig')->where('bids_pigs.id_bid',$id)->where('pigeons.deleted_at',null)->paginate(4);

 return view('cleint.AnnoncesClient.MESPG', compact('pgs'));

    }




    public function infobidspg($id){ // plus information bids pg

   $pigs = DB::table('pigeons')->where('deleted_at',null)->where('id_pig',$id)->get();

return view('cleint.infoAnnonces.infopgBD',compact('pigs'));

    }







}
