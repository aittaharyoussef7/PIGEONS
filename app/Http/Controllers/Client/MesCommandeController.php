<?php

namespace App\Http\Controllers\Client;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;
use App\Http\Controllers\Controller;

class MesCommandeController extends Controller
{
     public function __construct(){
  $this->middleware('auth');

   
 }




public function Mescmd(){    // view recupéer Commande

  $cmd = DB::table('commandes')->where('deleted_at',null)->orderBy('id_cmd', 'desc')->where('id_user',Auth::user()->id)->paginate(5);

 return view ('cleint.AnnoncesClient.cmdvalider',compact('cmd'));

}



/////////////////////////////////////////////Resevoire Commande /////////////////////////////////////////////////
 public  function MescmdRev(){

 	 $cmd = DB::table('commandes')->where('cmd_client_id',Auth::user()->id)->where('commandes.deleted_at',null)->paginate(5);


return view ('cleint.commandes.RevCmd',compact('cmd'));
 }






public function AnnulerC($id , $pre_id){ 
$cmds =DB::table('commandes')->where('id_cmd' ,$id)->get();
foreach ($cmds as $cmd) {

if($cmd->Annuler==1){

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['Annuler' => 0]);
} else {

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['Annuler' => 1]);

}
}
 $cmd = DB::table('commandes')->where('commandes.id_cmd','<=',$pre_id)->orderBy('id_cmd', 'desc')->where('id_user',Auth::user()->id)->where('commandes.deleted_at',null)->paginate(5);
                
      
   
            return view('cleint.ajax.tablevalider' , compact('cmd'));
 }







public function valider($id , $pre_id){ 
$cmd =DB::table('commandes')->where('id_cmd' ,$id)->get();
if($cmd[0]->etat_cmd=='non_valider'){

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['etat_cmd' => 'valider']);
} else {

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['etat_cmd' => 'non_valider']);

}

 $cmd = DB::table('commandes')->where('commandes.id_cmd','>=',$pre_id)->where('cmd_client_id',Auth::user()->id)->where('commandes.deleted_at',null)->paginate(5);
                
      
   
            return view('ajax.tablecmdvalider' , compact('cmd'));
 }

public function deletcmd($id , $pre_id){ 
DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['deleted_at' => Carbon::now()]);
            
 $cmd = DB::table('commandes')->where('commandes.id_cmd','>=',$pre_id)->where('cmd_client_id',Auth::user()->id)->where('commandes.deleted_at',null)->paginate(5);
               return view('ajax.tablecmdvalider' , compact('cmd'));
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


public function deletcmdCENV($id , $pre_id){ 
DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['deleted_at' => Carbon::now()]);
            
 $cmd = DB::table('commandes')->where('commandes.id_cmd','<=',$pre_id)->orderBy('id_cmd', 'desc')->where('id_user',Auth::user()->id)->where('commandes.deleted_at',null)->paginate(5);
                   return view('cleint.ajax.tablevalider' , compact('cmd'));
}




}
