<?php

namespace App\Http\Controllers\Client;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\User;
use App\Commandes;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use App\Contenirpr;
use Carbon\Carbon;
use App\Participer;
use App\Supportpgn;
use Mail;
use App\Adressel;
use App\Http\Controllers\Controller;

class PasserCommandeController extends Controller
{


       public function __construct(){
       		if(!isset($_SESSION)){session_start();}
if(!isset($_SESSION['panier']['produits'])){$_SESSION['panier']['produits']=array();}
   if(!isset($_SESSION['panier']['pigeons'])){$_SESSION['panier']['pigeons']=array();}
  $this->middleware('auth');

   
 }


 public function StoreCmd(Request $request){


    $add = new Adressel();
       $add->id_user=Auth::user()->id;
    $add->nom=$request->input('nom');
    $add->prenom=$request->input('prenom');
$add->tel=$request->input('tel');
$add->Adresse=$request->input('adresse');

 $add->ville=$request->input('ville');
  $add->code_postal=$request->input('postal');
    $add->Rue=$request->input('Rue');
 $add->save();
$totalPG=0; $totalPR=0;
    $cmd = new Commandes();
       $cmd->etat_cmd="non_valider";
    $cmd->id_user=Auth::user()->id;
      $cmd->id_liv=$add->id;
    
$cmd->save();
$id_cmd = DB::table('commandes')->max('id_cmd');


foreach ($_SESSION['panier']['pigeons'] as $key => $id_pig) {
     

    $contPG = new Contenirpgn();
       $contPG->id_cmd=$id_cmd ;
  $contPG->qnt=$request->input('PG'.$id_pig);
$contPG->id_pig=$id_pig;
$pigs = DB::table('pigeons')->where('id_pig',$id_pig)->get();
foreach ($pigs as $prix_pgn) {
$totalPG=$totalPG+$prix_pgn->prix*$contPG->qnt;
 if(Auth::user()->admin==0){
        $contPG->id_user=$prix_pgn->id_user;
      }
if(isset($contPG)){
  $contPG->save();
  }
}

    }

    foreach ($_SESSION['panier']['produits'] as $key => $id_pro) {
    
    $contPR = new Contenirpr();
       $contPR->id_cmd=$id_cmd ;
  $contPR->qnt=$request->input('PR'.$id_pro);
$contPR->id_pro=$id_pro;
$pros = DB::table('produits')->where('id_pro',$id_pro)->get();
foreach ($pros as  $prix_pro) {
$totalPR=$totalPR+$prix_pro->prix_pro*$contPR->qnt;

if(Auth::user()->admin==0){
        $contPR->id_user=$prix_pro->id_user;
      }
}

 

if(isset($contPR)){
  $contPR->save();
}



    }




DB::table('commandes')
            ->where('id_cmd',$id_cmd)
            ->update(['prix_total' => $totalPR+$totalPG , 
            	'checksum' => str_random(200)]);


    $user = User::findOrFail(Auth::user()->id);


 $cmds=DB::table('commandes')->where('id_cmd',$id_cmd)->get();
 Mail::send('cleint.confirmeCmd', ['user' => $user , 'cmds'=>$cmds], function ($m) use ($user) {
            $m->from('aittaharyoussef7@gmail.com', 'Confirmatoion Commande');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    
 session()->flash('storecmd' ,' Votre Annonces  à été bien supprimer ');
 return redirect("/");
	# code...
}

 public function confirmercmd( $id_cmd, $checksum){
DB::table('commandes')
            ->where('checksum',$checksum)
            ->update(['confirme' => 1 
            	]);
            unset($_SESSION['panier']['produits']);
 unset($_SESSION['panier']['pigeons']);
  session()->flash('confirmercmd' ,' Votre Annonces  à été bien supprimer ');
            return redirect("/");
            
}









 public function StorePar(Request $request){


    $newpar = new Participer();
       $newpar->id_user=Auth::user()->id;
    $newpar->nom=$request->input('nom');
    $newpar->prenom=$request->input('prenom');
$newpar->tel=$request->input('tel');
$newpar->Adresse=$request->input('adresse');
 $newpar->ville=$request->input('ville');
  $newpar->code_postal=$request->input('postal');
  $newpar->email=$request->input('email');

   $newpar->prix_par=$request->input('prix');
     $newpar->id_pig=$request->input('id_pig');
$newpar->checksum=str_random(200);

 $newpar->save();
   $user = User::findOrFail(Auth::user()->id);
$id_par = DB::table('participers')->max('id_par');

  $pars=DB::table('participers')->where('id_par',$id_par)->get();
 Mail::send('cleint.ConfirmePar', ['user' => $user , 'pars'=>$pars], function ($m) use ($user) {
            $m->from('aittaharyoussef7@gmail.com', 'Confirmatoion Participer');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    
 session()->flash('storecmd' ,' Votre Annonces  à été bien supprimer ');
 return redirect("/");


}







 public function ConfirmerPar( $id_par, $checksum){
DB::table('participers')
            ->where('checksum',$checksum)
            ->update(['confirmer' => 1 
              ]);
         session()->flash('confirmercmd' ,' Votre Annonces  à été bien supprimer ');
            return redirect("/");
            
}




}