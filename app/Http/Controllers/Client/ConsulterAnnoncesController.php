<?php

namespace App\Http\Controllers\Client;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;
use App\Http\Controllers\Controller;


class ConsulterAnnoncesController extends Controller
{
    






   public  function recherech($nom){

 	 $pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('nom_pgn', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->where('bid',0)->paginate(5);
   

    	 $prs = DB::table('produits')->where('deleted_at',null)->where('status',1)->where('nom_pro', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->paginate(5);


    	  	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('nom_bid', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->paginate(5);



$noms = array('nom' => $nom );


return view ('cleint.ajax.recherche',compact(['pgs','bids','prs','noms']));
}




   public  function TousRecherche($nom){

 	 $pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('nom_pgn', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->where('bid',0)->get();
   

    	 $prs = DB::table('produits')->where('deleted_at',null)->where('status',1)->where('nom_pro', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->get();


    	  	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('nom_bid', 'like', '%'.$nom."%") ->orWhere('description', 'like', '%'.$nom."%")->get();



$noms = array('nom' => $nom );


return view ('cleint.TousRecherche',['pgs' => $pgs , 'prs'=>$prs , 'bids'=>$bids]);
 }





















     public  function pigeons(){

 	 $pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('bid',0)->paginate(5);


return view ('cleint.pigeons.pigeons',compact('pgs'));
 }



     public  function produits(){

 	 $prs = DB::table('produits')->where('deleted_at',null)->where('status',1)->paginate(10);


return view ('cleint.produits.produits',compact('prs'));
 }



     public  function bids(){

 	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('date_f','>=',Carbon::now())->paginate(5);


return view ('cleint.bids.bids',compact('bids'));
 }

 public  function FiltreBD(Request $request ){ 




	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('date_f','>=',Carbon::now())->where('nom_bid', 'like', '%'.$request->input('nom').'%')->paginate(5);

	 return view ('cleint.bids.bids',compact('bids'));


}
 public  function FiltreBDDATE($date){ 

if($date==1){


	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('date_f','>=',Carbon::now())->

	 orderBy('date_f')->paginate(5);

	 return view ('cleint.bids.bids',compact('bids'));}

else {
	 $bids = DB::table('bids')->where('deleted_at',null)->where('status',1)->where('date_f','>=',Carbon::now())->

	 orderBy('date_f',"desc")->paginate(5);

	 return view ('cleint.bids.bids',compact('bids'));
}}




 public  function FiltreCAT(Request $request ){
 
 $typeD=$request->input('distance');
 $typeC=$request->input('id_cat');


if($typeC !=0){if( $typeD =="TOUS"){ 
	$pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('id_cat',$request->input('id_cat') )->where('bid',0)->paginate(5); 
	return view ('cleint.pigeons.pigeons',compact('pgs'));
}}


if($typeD !="TOUS"){if( $typeC ==0){  $pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('distance',$request->input('distance') )->where('bid',0)->where('bid',0)->paginate(5);


		return view ('cleint.pigeons.pigeons',compact('pgs'));
}}



if($typeC !=0){if($typeD!=null){
	$pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('id_cat',$request->input('id_cat') )->where('distance',$request->input('distance') )->where('bid',0)->paginate(5);
	return view ('cleint.pigeons.pigeons',compact('pgs'));
}

}


if($typeC ==0 ){if($typeD =="TOUS"){
	 $pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->where('bid',0)->paginate(5);


return view ('cleint.pigeons.pigeons',compact('pgs'));

}}

}

 public  function FiltrePRIXPG(Request $request ){


	$pgs = DB::table('pigeons')->where('deleted_at',null)->where('status',1)->whereBetween('prix', [$request->input('min') ,$request->input('max')])->where('bid',0)->paginate(5);


	return view ('cleint.pigeons.pigeons',compact('pgs'));


}


 public  function FiltrePRIXPR(Request $request ){


	$prs = DB::table('produits')->where('deleted_at',null)->where('status',1)->whereBetween('prix_pro', [$request->input('min') ,$request->input('max')])->paginate(1);


	return view ('cleint.produits.produits',compact('prs'));


}



}


