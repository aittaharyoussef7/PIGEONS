<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;

use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;
class AnnoncesController extends Controller
{


 public function __construct(){
  $this->middleware('auth');

   
 }


public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  
return true;
}


public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}



public function PolicyCleintPG(){
  if(Auth::user()->admin==0){
    $tests=  DB::table('visiteur')->get();
    foreach($tests as  $test){
      if($test->authclientPG==0) 
      return abort(404); 
    
  }
}}










public function PolicyCleintPR(){
  if(Auth::user()->admin==0){
    $tests=  DB::table('visiteur')->get();
    foreach($tests as  $test){
      if($test->authclientPR==0) 
      return abort(404); 
    
  }
}}






public function PolicyCleintBD(){
  if(Auth::user()->admin==0){
    $tests=  DB::table('visiteur')->get();
    foreach($tests as  $test){
      if($test->authclientBD==0) 
      return abort(404); 
    
  }
}}




 public function CreatePG(){
   $this->PolicyCleintPG();


 if(Auth::user()->admin==0){
     return view('CreateEditAnnoces.CreatePG');}
     else {
 return view('admin.newAnnonces.CreatePG');}
     }

  



 public function CreatePR(){
   $this->PolicyCleintPR();

 if(Auth::user()->admin==0){
     return view('CreateEditAnnoces.CreatePR');}
     else {
 return view('admin.newAnnonces.CreatePR');}
     }
 
  




 public function CreateBD(){
  


 
 if(Auth::user()->admin==0){
     return view('CreateEditAnnoces.CreateBD');}
     else {
 return view('admin.newAnnonces.CreateBD');}
     }
 



public function PolicyPigeon($id){
 $tests=  DB::table('pigeons')->where('id_pig',$id)->get();


    foreach($tests as  $test){
      if($test->id_user!=Auth::user()->id) 
      return abort(404); 
    
} return true;

}




public function PolicyProduit($id){
$tests=  DB::table('produits')->where('id_pro',$id)->get();


    foreach($tests as  $test){
      if($test->id_user!=Auth::user()->id) 
      return abort(404); 
    
}return true;}




public function Policybids($id){
 $tests=  DB::table('bids')->where('id_bid',$id)->get();


    foreach($tests as  $test){
      if($test->id_user!=Auth::user()->id) 
      return abort(404); 
    
}return true;}





 public function EditBD($id){
   

$this->Policybids($id);

  $bids= DB::table('bids')
            ->where('id_bid',$id)->get();
         
             if(Auth::user()->admin==0){
      return view('CreateEditAnnoces.EditBD',compact('bids'));}
     else {
     return view('admin.newAnnonces.EditBD',compact('bids'));}
     }

 

  



 public function EditPG($id){

  $this->PolicyPigeon($id);
  $pgs= DB::table('pigeons')
            ->where('id_pig',$id)->get();
         
            
   if(Auth::user()->admin==0){
      return view('CreateEditAnnoces.EditPG',compact('pgs'));}
     else {
     return view('admin.newAnnonces.EditPG',compact('pgs'));}
 
   
  }



 public function EditPR($id){
$this->PolicyProduit($id);
  $prs= DB::table('produits')
            ->where('id_pro',$id)->get();
         
            if(Auth::user()->admin==0){
     return view('admin.CreateEditAnnoces.EditPR',compact('prs'));}
     else {
     return view('admin.newAnnonces.EditPR',compact('prs'));}

 
   
  }



 public function StorePG(RequestPG $request){




    $pg = new pigeon();
       $pg->id_user=Auth::user()->id;
    $pg->nom_pgn=$request->input('nom');
$pg->description=$request->input('des');
$pg->sexe=$request->input('sexe');
$pg->prix=$request->input('prix');
$pg->distance=$request->input('dis');
$pg->qnt_pgn=$request->input('qnt');
$pg->id_cat=$request->input('id_cat');

   $pg->welcome=$request->input('welcome');
   if($request->input('promotion')==null){$pg->promotion=100;}else{
$pg->promotion=$request->input('promotion');}


if($request->input('id_bid')){
  $pg->bid=1;
 $pg->prix=0;
 $pg->promotion=0;
}

 if(Auth::user()->admin==1) { $pg->status=1; $pg->admin=1;}




 $pg->save();
 $derPg = DB::table('pigeons')
           ->max('id_pig');

$n=$derPg;


    $dossier = 'photo_pig'.$n;


if(!is_dir($dossier)){
   mkdir($dossier);
}



  if($request->file('photo1') != null ){
    $sup = new Supportpgn();
$photo = $request->file('photo1')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo1')->move($destination, $photo);
$image=str_replace('\\', '/',$image);

$sup->photo=$image;
$sup->id_pig=$n;
$sup->save();

}


  if($request->file('photo2') != null ){
    $sup = new Supportpgn();
$photo = $request->file('photo2')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo2')->move($destination, $photo);
$image=str_replace('\\', '/',$image);
$sup->photo=$image;
$sup->id_pig=$n;
$sup->save();

}



  if($request->file('photo3') != null ){
    $sup = new Supportpgn();
$photo = $request->file('photo3')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo3')->move($destination, $photo);

$image=str_replace('\\', '/',$image);
$sup->photo=$image;
$sup->id_pig=$n;
$sup->save();

}



  if($request->file('photo4') != null ){
    $sup = new Supportpgn();
$photo = $request->file('photo4')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo4')->move($destination, $photo);
$image=str_replace('\\', '/',$image);
$sup->photo=$image;
$sup->id_pig=$n;
$sup->save();

}

if($request->input('id_bid')){

 $bdpg = new Bids_pig();
    $bdpg->id_bid=$request->input('id_bid');
$bdpg->id_pig=$n;

 $bdpg->save();

 if($request->input('terminer')){if(Auth::user()->admin==1){
 session()->flash('sucPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('MESPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/MESPG');}
 }

   session()->flash('sucBDPG' ,' Votre Annonces  à été bien supprimer ');
 
 if(Auth::user()->admin==0){
 return view('CreateEditAnnoces.CreatePG' ,['id_bid'=>$request->input('id_bid')]);}
 else {
   return view('admin.newAnnonces.CreatePG' ,['id_bid'=>$request->input('id_bid')]);}
 }



if(Auth::user()->admin==1){
 session()->flash('sucPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('MESPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/MESPG');}
 }
  




 public function UpdatePG($id ,Request $request){
  session()->flash('MODPG' ,' Votre Annonces  à été bien supprimer ');
  if(Auth::user()->admin==0){$welcome=0;}else{
    $welcome=$request->input('welcome');
  }
if($request->input('promotion')==0){$promo=100;}else {
  $promo=$request->input('promotion');
}
DB::table('pigeons')
            ->where('id_pig', $id)
            ->update(['nom_pgn' => $request->input('nom'),

'prix' => $request->input('prix'),
'qnt_pgn' => $request->input('qnt'),
'description' => $request->input('des'),
'distance' => $request->input('dis'),
'id_cat' => $request->input('id_cat'),
'welcome' => $welcome,
'promotion' =>  $promo,

          ]);

 $sups =DB::table('supportpgns')
            ->where('id_pig', $id)->get(); 
foreach ($sups as $sup) {


  if($request->file($sup->id_sup) != null ){

     $dossier = 'photo_pig'.$id;


if(!is_dir($dossier)){
   mkdir($dossier);
 


}



$photo = $request->file($sup->id_sup)->getClientOriginalName();
$destination = $dossier;
$image =$request->file($sup->id_sup)->move($destination, $photo);
$image=str_replace('\\', '/',$image);

DB::table('supportpgns')
            ->where('id_sup', $sup->id_sup)
            ->update(['photo' => $image,







          ]);   }


          }




if(Auth::user()->admin==1){
 session()->flash('ModSUC' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('ModSUC' ,' Votre Annonces  à été bien supprimer ');
return redirect('/MESPG');}
          }

 public function UpdatePR($id ,Request $request){$this->PolicyProduit($id);
    session()->flash('MODPR' ,' Votre Annonces  à été bien supprimer ');
    if(Auth::user()->admin==0){$welcome=0;}else{
    $welcome=$request->input('welcome');
  }
if($request->input('promotion')==0){$promo=100;}else {
  $promo=$request->input('promotion');
}


DB::table('produits')
            ->where('id_pro', $id)
            ->update(['nom_pro' => $request->input('nom'),

'description' => $request->input('des'),
'prix_pro' => $request->input('prix'),
'description' => $request->input('des'),
'qnt_pro' => $request->input('qnt'),
'welcome' => $welcome,
'promotion' => $promo,
          ]);

  if($request->file("photo1") != null ){
  
     $dossier = 'photo_pro'.$id;


if(!is_dir($dossier)){
   mkdir($dossier);
}

$photo = $request->file("photo1")->getClientOriginalName();
$destination = $dossier;
$image =$request->file("photo1")->move($destination, $photo);
$image=str_replace('\\', '/',$image);
DB::table('produits')
            ->where('id_pro', $id)
            ->update(['photo_pro' => $image,]);}




if(Auth::user()->admin==1){
 session()->flash('ModSUC' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('ModSUC' ,' Votre Annonces  à été bien supprimer ');
return redirect('/MESPR');}

          }



 public function StorePR(RequestPR $request){

  if(Auth::user()->admin==0){$welcome=0;}else{
    $welcome=$request->input('welcome');
  }
if($request->input('promotion')==null){$promo=100;}else{
  $promo=$request->input('promotion');
}


 $derPr = DB::table('produits')
          ->max('id_pro');             
$n=$derPr+1;
    $dossier = 'photo_pro'.$n;
if(!is_dir($dossier)){
   mkdir($dossier);
}


    $pr = new Produit();
     $pr->id_user=Auth::user()->id;
    $pr->nom_pro=$request->input('nom');
$pr->description=$request->input('des');
$pr->prix_pro=$request->input('prix');
if(Auth::user()->admin==1) { $pr->status=1; $pr->admin=1;}
$pr->qnt_pro=$request->input('qnt');

   $pr->welcome=$welcome;
$pr->promotion=$promo;


  if($request->file('photo1') != null ){

$photo = $request->file('photo1')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo1')->move($destination, $photo);

$image=str_replace('\\', '/',$image);
$pr->photo_pro=$image;


}

    $pr->save();


if(Auth::user()->admin==1){
 session()->flash('sucPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('MESPR' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/MESPR');}
}



 public function UpdateBD($id ,Request $request){
    session()->flash('MODBD' ,' Votre Annonces  à été bien supprimer ');
$this->Policybids($id);

DB::table('bids')
            ->where('id_bid', $id)
            ->update(['nom_bid' => $request->input('nom'),

'description' => $request->input('des'),
'date_d' => $request->input('date_d'),
'description' => $request->input('des'),
'date_f' => $request->input('date_f'),
'qnt_pig' => $request->input('qnt_pig'),
'welcome' => $request->input('welcome'),

          ]);


  if($request->file("photo1") != null ){
  
     $dossier = 'photo_pro'.$id;


if(!is_dir($dossier)){
   mkdir($dossier);
}



$photo = $request->file("photo1")->getClientOriginalName();
$destination = $dossier;
$image =$request->file("photo1")->move($destination, $photo);
$image=str_replace('\\', '/',$image);
DB::table('bids')
            ->where('id_bid', $id)

            ->update(['photo_bids' => $image,]);}if(Auth::user()->admin==1){
 session()->flash('sucPG' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/admin');}
 else {
  session()->flash('MESPR' ,' Votre Annonces  à été bien supprimer ');
 return redirect('/MESPR');}}






 public function StoreBD(RequestBD $request){


 $derPr = DB::table('bids')
           ->max('id_bid');
                
$n=$derPr+1;

    $dossier = 'BIDS'.$n;
if(!is_dir($dossier)){
   mkdir($dossier);
}

 session()->flash('sucBD' ,' Votre Annonces  à été bien supprimer ');

    $bd = new Bid();
     $bd->id_user=Auth::user()->id;
     if(Auth::user()->admin==1)
 $bd->welcome=$request->input('welcome');
    $bd->nom_bid=$request->input('nom');
$bd->description=$request->input('des');
$bd->date_d=$request->input('date_d');

$bd->date_f=$request->input('date_f');
$bd->qnt_pig=$request->input('qnt_pig');
if(Auth::user()->admin==1) { $bd->status=1; $bd->admin=1;}
  if($request->file('photo1') != null ){

$photo = $request->file('photo1')->getClientOriginalName();
$destination = $dossier;
$image =$request->file('photo1')->move($destination, $photo);
$image=str_replace('\\', '/',$image);
$bd->photo_bids=$image;


}

    $bd->save();

 $derbd = DB::table('bids')
           ->max('id_bid');
 


 $bids = DB::table('bids')
         ->where('id_bid',$derbd)->get();

if(Auth::user()->admin==0){
 return view('CreateEditAnnoces.CreatePG' ,['id_bid'=>$bids[0]->id_bid]);
}else {
   return view('admin.newAnnonces.CreatePG' ,['id_bid'=>$bids[0]->id_bid]);
}
}








public function deletePGC($id , $pre_id){ 
$pg =DB::table('pigeons')->where('id_pig' ,$id)->get(); 
if($pg[0]->status==1){

DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['deleted_at' => Carbon::now()]);

            $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('id_pig','<=',$pre_id)->where('id_user',Auth::user()->id)->where('pigeons.status',1)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(4);
} else {


DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['deleted_at' => Carbon::now()]);

    $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('id_pig','<=',$pre_id)->where('id_user',Auth::user()->id)->where('pigeons.status',0)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(4);
               
}

           
       
               return view('cleint.ajax.PG', compact('pgs'));
            
}





public function deletePRC($id , $pre_id){ 
$pr =DB::table('produits')->where('id_pro' ,$id)->get(); 
if($pr[0]->status==1){

DB::table('produits')
            ->where('id_pro',$id)
            ->update(['deleted_at' => Carbon::now()]);

            $prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('id_pro','<=',$pre_id)->where('id_user',Auth::user()->id)->where('produits.status',1)->orderBy('id_pro', 'desc')->paginate(4);
} else {


DB::table('produits')
            ->where('id_pro',$id)
            ->update(['deleted_at' => Carbon::now()]);

     $prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('id_pro','<=',$pre_id)->where('id_user',Auth::user()->id)->where('produits.status',0)->orderBy('id_pro', 'desc')->paginate(4);
               
}

           
       
               return view('cleint.ajax.PR', compact('prs'));
            
}


































public function deletePG($id , $pre_id){if($this->policyAdmin()==true){ $i=1; /*zaydha o safi */}
else { $this->PolicyPigeon($id);}
$pg =DB::table('pigeons')->where('id_pig' ,$id)->get();
if($pg[0]->status==1){

DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['deleted_at' => Carbon::now()]);

            $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('id_pig','<=',$pre_id)->where('pigeons.status',1)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(5);
} else {


DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['deleted_at' => Carbon::now()]);

    $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('id_pig','<=',$pre_id)->where('pigeons.status',0)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(5);
                
}


      
   
            return view('ajax.tablePG' , compact('pgs'));
}







public function MESBD(){

 $bids =DB::table('bids')->where('deleted_at',null)->where('id_user',Auth::user()->id)->orderBy('id_bid', 'desc')->paginate(4);
 return view('cleint.AnnoncesClient.MESBD', compact('bids'));
}



public function MESPG(){

 $pgs =DB::table('pigeons')->where('deleted_at',null)->where('bid',0)->where('id_user',Auth::user()->id)->orderBy('id_pig', 'desc')->paginate(4);
 return view('cleint.AnnoncesClient.MESPG', compact('pgs'));
}
public function MESPR(){

 $prs =DB::table('produits')->where('deleted_at',null)->where('id_user',Auth::user()->id)->orderBy('id_pro', 'desc')->paginate(4);
 return view('cleint.AnnoncesClient.MESPR', compact('prs'));
}


public function deletePR($id , $pre_id){if($this->policyAdmin()==true){$i=0;/*3i zaydha */}

else {$this->PolicyProduit($id);}
$pr =DB::table('produits')->where('id_pro' ,$id)->get();
if($pr[0]->status==1){

DB::table('produits')
            ->where('id_pro',$id)
            ->update(['deleted_at' =>Carbon::now() ]);

             $prs = DB::table('produits')->where('produits.id_pro','<=',$pre_id)->where('status',1)->where('produits.deleted_at',null)->orderBy('id_pro', 'desc')->paginate(5);
} else {


DB::table('produits')
            ->where('id_pro',$id)
            ->update(['deleted_at' => Carbon::now()]);

             $prs = DB::table('produits')->where('produits.id_pro','<=',$pre_id)->where('status',0)->where('produits.deleted_at',null)->orderBy('id_pro', 'desc')->paginate(5);
}
       return view('ajax.tablePR' , compact('prs'));
 }

 



public function deleteBD($id , $pre_id){if($this->policyAdmin()==true){$i=0; /* 3i zaydha */}
else {
$this->Policybids($id);}

$bid =DB::table('bids')->where('id_bid' ,$id)->get();
if($bid[0]->status==1){

DB::table('bids')
            ->where('id_bid',$id)
            ->update(['deleted_at' =>Carbon::now() ]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',1)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
} else {


DB::table('bids')
            ->where('id_bid',$id)
            ->update(['deleted_at' => Carbon::now()]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',0)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
}

        return view('ajax.tableBD' , compact('bids'));}
 





public function deleteBDC($id , $pre_id){

$bid =DB::table('bids')->where('id_bid' ,$id)->get();
if($bid[0]->status==1){

DB::table('bids')
            ->where('id_bid',$id)
            ->update(['deleted_at' =>Carbon::now() ]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',1)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
} else {


DB::table('bids')
            ->where('id_bid',$id)
            ->update(['deleted_at' => Carbon::now()]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',0)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
}

        return view('cleint.ajax.BD' , compact('bids'));}

        
 }





