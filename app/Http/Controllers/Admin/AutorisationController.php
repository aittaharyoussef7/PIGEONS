<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;


class AutorisationController extends Controller
{
  public function __construct(){
  $this->middleware('auth');

   
 }
public function PolicySupAdmin(){          // policy Supureur Admin
    if(Auth::user()->sup_Admin==0) abort(404);  
}


public function policyAdmin(){ // policy  Admin




  if(Auth::user()->admin==0) abort(404);  

}

 public function authclient($id){$this->policyAdmin(); $this->PolicySupAdmin();  // Autoriser à le Cleint pour céer des Annonces
if($id==0){
          
DB::table('visiteur')
            ->update(['authclientPG' => 0]);      }
if($id==2){
            
DB::table('visiteur')
            ->update(['authclientPR' => 0]);     }

if($id==4){
DB::table('visiteur')
            ->update(['authclientBD' => 0]);
                  }
            


if($id==1){
          
DB::table('visiteur')
            ->update(['authclientPG' => 1]);      }
if($id==3){
            
DB::table('visiteur')
            ->update(['authclientPR' => 1]);     }

if($id==5){
DB::table('visiteur')
            ->update(['authclientBD' => 1]);
                  }
            





            return view('ajax.setting');
 }




 
 public function setting(){$this->policyAdmin(); // view setting Admin

 $this->PolicySupAdmin();

            return view('admin.setting.setting');
 }



 public function NPG(){$this->policyAdmin(); // Annonces NOn auth Pigeon

  $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.status',0)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(5);

            return view('admin.PG' , compact('pgs'));
 }




public function NPR(){$this->policyAdmin();// Annonces NOn auth produit
 $prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('produits.status',0)->orderBy('id_pro', 'desc')->paginate(5);
                
      
   
            return view('admin.PR' , compact('prs'));
 }




 public function APG(){$this->policyAdmin(); // Annonces  auth Pigeon
$pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.status',1)->orderBy('id_pig', 'desc')->where('bid',0)->paginate(5);

            return view('admin.PG' , compact('pgs'));
 }


 public function infoPG($id){$this->policyAdmin(); // plus information sur un PG
 $pgs = DB::table('pigeons')->where('id_pig',$id)->get();
         
                
      
   
            return view('admin.infoPG' , compact('pgs'));
 }

 public function infoPR($id){$this->policyAdmin(); // plus information sur un produit
 $prs = DB::table('produits')->where('id_pro',$id)->get();
         
                
      
   
            return view('admin.infoPR' , compact('prs'));
 }

 public function infoBD($id){$this->policyAdmin(); // plus information sur un bids
 $bids = DB::table('bids')->where('id_bid',$id)->get();
         
                
      
   
            return view('admin.PG' , compact('bids'));
 }






public function authPG($id , $pre_id){$this->policyAdmin();  // Authoriser PG
$pg =DB::table('pigeons')->where('id_pig' ,$id)->get();
if($pg[0]->status==1){

DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['status' => 0]);

           
$pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.id_pig','<=',$pre_id)->where('pigeons.status',1)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(5);

          
         

} else {


DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['status' => 1]);

 
      $pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.id_pig','<=',$pre_id)->where('pigeons.status',0)->where('bid',0)->orderBy('id_pig', 'desc')->paginate(5);

            
 
                
}


      
   
            return view('ajax.tablePG' , compact('pgs'));
 }



public function authPR($id , $pre_id){$this->policyAdmin(); // Authoriser PR
$pr =DB::table('produits')->where('id_pro' ,$id)->get();
if($pr[0]->status==1){

DB::table('produits')
            ->where('id_pro',$id)
            ->update(['status' => 0]);

             $prs = DB::table('produits')->where('produits.id_pro','<=',$pre_id)->where('status',1)->where('produits.deleted_at',null)->orderBy('id_pro', 'desc')->paginate(5);
} else {


DB::table('produits')
            ->where('id_pro',$id)
            ->update(['status' => 1]);

             $prs = DB::table('produits')->where('produits.id_pro','<=',$pre_id)->where('status',0)->where('produits.deleted_at',null)->orderBy('id_pro', 'desc')->paginate(5);
}


      
   
            return view('ajax.tablePR' , compact('prs'));
 }



 public function APR(){$this->policyAdmin();// List des PR autoriser
 $prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('produits.status',1)->orderBy('id_pro', 'desc')->paginate(5);
                
      
   
            return view('admin.PR' , compact('prs'));
 }


 public function Nbids(){$this->policyAdmin();// List des bids NON autoriser
 $bids = DB::table('bids')
            ->where('bids.deleted_at',null)->where('bids.status',0)->orderBy('id_bid', 'desc')->paginate(5);
                
      
   
            return view('admin.Bids' , compact('bids'));
 }

public function Abids(){$this->policyAdmin(); // List des BIDs autoriser
 $bids = DB::table('bids')
            ->where('bids.deleted_at',null)->where('bids.status',1)->orderBy('id_bid', 'desc')->paginate(5);
                
      
   
            return view('admin.Bids' , compact('bids'));
 }




public function authBD($id , $pre_id){$this->policyAdmin();// Authoriser BIDS
$bid =DB::table('bids')->where('id_bid' ,$id)->get();
if($bid[0]->status==1){

DB::table('bids')
            ->where('id_bid',$id)
            ->update(['status' =>0]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',1)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
} else {


DB::table('bids')
            ->where('id_bid',$id)
            ->update(['status' =>1]);

             $bids = DB::table('bids')->where('bids.id_bid','<=',$pre_id)->where('status',0)->where('bids.deleted_at',null)->orderBy('id_bid', 'desc')->paginate(5);
}


      
   
            return view('ajax.tableBD' , compact('bids'));
 }







 public function infobids($id){$this->policyAdmin(); //list des pigeon Bids

  $pgs = DB::table('bids')
->join('Bids_pigs', 'Bids_pigs.id_bid', '=', 'bids.id_bid')

->join('pigeons', 'pigeons.id_pig', '=', 'Bids_pigs.id_pig')

->where('bids.id_bid',$id)
            ->where('bids.deleted_at',null)->get();

            return view('admin.BDpg' , compact('pgs'));
 }






}
