<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;   ///////////////////////////// changer Autorisation a permer page welcome

class welcomeController extends Controller
{
   

   public function __construct(){
  $this->middleware('auth');

   
 }
public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}


public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  

}
public function welcomePG($id , $id_pre){

$pg =DB::table('pigeons')->where('id_pig' ,$id)->get();
if($pg[0]->welcome==1){


DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['welcome' => 0]);


          
         

} else {


DB::table('pigeons')
            ->where('id_pig',$id)
            ->update(['welcome' => 1]);

 
    
            
 
                
}



           if($pg[0]->status==1){
$pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.status',1)->where('id_pig','<=',$id_pre)->orderBy('id_pig', 'desc')->where('bid',0)->paginate(5);}
          if($pg[0]->status==0){
            	$pgs = DB::table('pigeons')
            ->where('pigeons.deleted_at',null)->where('pigeons.status',0)->where('id_pig','<=',$id_pre)->orderBy('id_pig', 'desc')->where('bid',0)->paginate(5);
            }
      
   
   
            return view('ajax.tablePG' , compact('pgs'));

}



public function welcomePR($id , $id_pre){

$pr =DB::table('produits')->where('id_pro' ,$id)->get();
if($pr[0]->welcome==1){


DB::table('produits')
            ->where('id_pro',$id)
            ->update(['welcome' => 0]);


          
         

} else {


DB::table('produits')
            ->where('id_pro',$id)
            ->update(['welcome' => 1]);

 
    
            
 
                
}



           if($pr[0]->status==1){
$prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('produits.status',1)->where('id_pro','<=',$id_pre)->orderBy('id_pro', 'desc')->paginate(5);}
          if($pr[0]->status==0){
            	$prs = DB::table('produits')
            ->where('produits.deleted_at',null)->where('produits.status',0)->where('id_pro','<=',$id_pre)->orderBy('id_pro', 'desc')->paginate(5);
            }
      
   
   
            return view('ajax.tablePR' , compact('prs'));

}




public function welcomeBD($id , $id_pre){

$bd =DB::table('bids')->where('id_bid' ,$id)->get();
if($bd[0]->welcome==1){


DB::table('bids')
            ->where('id_bid',$id)
            ->update(['welcome' => 0]);


          
         

} else {


DB::table('bids')
            ->where('id_bid',$id)
            ->update(['welcome' => 1]);

 
    
            
 
                
}



           if($bd[0]->status==1){
$bids = DB::table('bids')
            ->where('bids.deleted_at',null)->where('bids.status',1)->where('id_bid','<=',$id_pre)->orderBy('id_bid', 'desc')->paginate(5);}
          if($bd[0]->status==0){
            	$bids = DB::table('bids')
            ->where('bids.deleted_at',null)->where('bids.status',0)->where('id_bid','<=',$id_pre)->orderBy('id_bid', 'desc')->paginate(5);
            }
      
   
   
       return view('ajax.tableBD' , compact('bids'));

}
}