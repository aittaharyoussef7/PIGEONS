<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Http\Requests\RequestSetting;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use Mail;
use App\Bid;
use App\User;
use App\Supportbdpg;
use App\Bids_pig;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Supportpgn;

class SettingController extends Controller
{
 public function __construct(){
  $this->middleware('auth');

   
 }


public function store_contact(Request $request){

    $user = User::findOrFail(Auth::user()->id);

session(['message' => $request->input('message') ]);
session(['tel' => $request->input('tel') ]);
session(['sujet' => $request->input('sujet') ]);
session(['email' => $request->input('email') ]);
session(['name' => $request->input('name') ]);
 Mail::send('cleint.MailContact', ['requests'=>$request], function ($m) use ($user) {
            $m->from('aittaharyoussef7@gmail.com', 'Confirmatoion Commande');

            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    
 session()->flash('contact' ,' Votre Annonces  à été bien supprimer ');
return redirect('/');

}


public function ModifierLogin(){
  if(Auth::user()->admin==0)
    return view('ModifierLogin'); 
    if(Auth::user()->admin==1)
       return view('admin.compte'); 
}

public function MOdifierLOGIN2(RequestSetting $request){

if($request->input('email')==null){$email=Auth::user()->email;}else {
  $email=$request->input('email');
}

$testMail=DB::table('users')->where('email',$email)->where('id','!=',Auth::user()->id)->get();
if($testMail!=null)

{
   session()->flash('email' ,' Votre Annonces  à été bien supprimer ');
return redirect('/ModifierLogin');
}
DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['email' =>$request->input('email'),

             'name' => $request->input('name'),
             'password' => Hash::make($request->input('password'))


          ]);
            if(Auth::user()->admin==1) return redirect('/admin');
              else return redirect('/');

}




public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  

}
public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}


public function Add_AdminMail($email){$this->PolicySupAdmin();
$user = DB::table('users')
            ->where('email',$email)->get();
            
       if($user[0]->admin==1){
  session()->flash('admin' ,' Votre Catégorie à été bien supprimer ');
  }

        elseif($user==null){
  session()->flash('no_email' ,' Votre Catégorie à été bien supprimer ');
            }

            else{       session()->flash('suc' ,' Votre Catégorie à été bien supprimer ');        }


DB::table('users')
            ->where('email',$email)
            ->update(['admin' =>1]);

 return view('ajax.setting');




}

public function deleteAdmin($id){$this->PolicySupAdmin();
DB::table('users')
            ->where('id',$id)
            ->update(['admin' =>0]);


 return view('ajax.setting');


}



public function Add_admin(){$this->PolicySupAdmin();



$this->policyAdmin();

return view('admin.setting');

}


public function updateinfo($email , $tel , $adresse , $tva){$this->PolicySupAdmin();




DB::table('visiteur') ->update(['Email' => $email , 'tel' =>$tel , 'adresse'=>$adresse , 'tva'=>$tva]);
 return view('ajax.setting');
}





public function colorM($colorM){$this->PolicySupAdmin();


DB::table('visiteur') ->update(['colorM' => '#'.$colorM]);
 return view('ajax.colorM');


}


public function colorN($colorN){$this->PolicySupAdmin();


DB::table('visiteur') ->update(['colorN' => '#'.$colorN]);
 return view('ajax.colorN');


}




}