<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;


class FactureController extends Controller
{
  


   public function __construct(){
  $this->middleware('auth');

   
 }

public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  

}
public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}




  
  public function downloadfacturePDF($id){ //Facture Admin Pigeon



 $cmdpg = DB::table('commandes')



->join('contenirpgns', 'commandes.id_cmd', '=', 'contenirpgns.id_cmd')


 ->join('pigeons', 'pigeons.id_pig', '=', 'contenirpgns.id_pig')


 ->where('commandes.id_cmd',$id)
 ->get(); 

  $cmdpr = DB::table('commandes')




->join('contenirprs', 'commandes.id_cmd', '=', 'contenirprs.id_cmd')


  ->join('produits', 'produits.id_pro', '=', 'contenirprs.id_pro')->where('commandes.id_cmd',$id)


 ->get(); 




$pdf = PDF::loadView('admin.facture', compact('cmdpg') , compact('cmdpr'));
      return $pdf->download('Facture.pdf');

    }









public function downloadfactureBDPDF($id){//Facture Admin bids

   $pars = DB::table('users')
->join('participers', 'participers.id_user', '=', 'users.id')

->join('pigeons', 'pigeons.id_pig', '=', 'participers.id_pig')

->where('participers.id_par',$id)
            ->where('pigeons.deleted_at',null)->get();


$pdf = PDF::loadView('admin.FactureBD', compact('pars'));
      return $pdf->download('FactureBD.pdf');

    }














}
