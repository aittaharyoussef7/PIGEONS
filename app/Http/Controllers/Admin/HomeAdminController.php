<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;

use App\Http\Controllers\Controller;
class HomeAdminController extends Controller
{
   public function __construct(){
  $this->middleware('auth');

   
 }

public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  

}
public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}



 public function home(){



$this->policyAdmin();
return view('admin.home');

}



 public function categorie(){$this->policyAdmin(); 
 $cat = DB::table('categories')
            ->where('deleted_at',null)->get();
                
      
   
            return view('admin.categorie' , compact('cat'));
 }

  public function Storecat($nom){$this->policyAdmin();
    $cat = new Categorie();
    $cat->nom_cat=$nom;

    $cat->save();


    return view('ajax.tablecategorie');
  }


 public function deletecat($id){$this->policyAdmin();
   
   DB::table('categories')
            ->where('id_cat',$id)
            ->update(['deleted_at' => Carbon::now()]);
            

  session()->flash('del' ,' Votre Catégorie à été bien supprimer ');
    return view('ajax.tablecategorie');
  }



  


}
