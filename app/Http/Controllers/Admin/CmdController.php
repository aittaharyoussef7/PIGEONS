<?php

namespace App\Http\Controllers\Admin;


use DB;
use Auth;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests\RequestPG;
use App\Http\Requests\RequestPR;
use App\Http\Requests\RequestBD;
use App\Pigeon;
use App\Produit;
use App\Categorie;
use App\Commade;
use App\Contenirpgn;
use App\Bid;
use App\Supportbdpg;
use App\Bids_pig;
use Carbon\Carbon;
use App\Supportpgn;
use App\Http\Controllers\Controller;
class CmdController extends Controller
{
 public function __construct(){
  $this->middleware('auth');

   
 }
 public function PolicySupAdmin(){
    if(Auth::user()->sup_Admin==0) abort(404);  
}

public function policyAdmin(){




  if(Auth::user()->admin==0) abort(404);  

}





public function recuperercmd(){ $this->policyAdmin();   // view recupéer Commande

  $cmd = DB::table('commandes')->where('deleted_at','!=',null)->where('confirme',1)->orderBy('id_cmd', 'desc')->paginate(5);

  return view('admin.recuperercmd' , compact('cmd'));

}


public function recuperercmdid($id ,$pre_id){ $this->policyAdmin(); // fonction récuper commande


DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['deleted_at' => null]);



  $cmd = DB::table('commandes')->where('deleted_at','!=',null)->where('confirme',1)->where('id_cmd','<=',$pre_id)->orderBy('id_cmd', 'desc')->paginate(5);

  return view('ajax.tablerecuperercmd' , compact('cmd'));

}





public function valider($id , $pre_id){ $this->policyAdmin(); // changer status etat cmd 
$cmd =DB::table('commandes')->where('id_cmd' ,$id)->get();
if($cmd[0]->etat_cmd=='non_valider'){

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['etat_cmd' => 'valider']);
} else {

DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['etat_cmd' => 'non_valider']);

}

 $cmd = DB::table('commandes')->where('commandes.id_cmd','<=',$pre_id)->where('confirme',1)->where('commandes.deleted_at',null)->orderBy('id_cmd', 'desc')->paginate(5);
                
      
   
            return view('ajax.tablecmdvalider' , compact('cmd'));
 }

public function deletcmd($id , $pre_id){ $this->policyAdmin(); // delete cmd
DB::table('commandes')
            ->where('id_cmd',$id)
            ->update(['deleted_at' => Carbon::now()]);
            
 $cmd = DB::table('commandes')->where('commandes.id_cmd','<=',$pre_id)->where('confirme',1)->where('commandes.deleted_at',null)->orderBy('id_cmd', 'desc')->paginate(5);
               return view('ajax.tablecmdvalider' , compact('cmd'));
}

   

 /* function qui lister les commande de pigeons  */


 public function RechercherCmd(Request $request){ $this->policyAdmin(); //rechercher cmd par id
 $cmd = DB::table('commandes')
            ->where('commandes.deleted_at',null)->where('id_cmd',$request->input('id'))->where('confirme',1)->orderBy('id_cmd', 'desc')->paginate(5);
               
      
   
            return view('admin.CmdPg' , compact('cmd'));
 }

 public function RechercherRecupCmd(Request $request){ $this->policyAdmin();//rechercher cmd par id recuperer
 $cmd = DB::table('commandes')
            ->where('commandes.deleted_at','!=',null)->where('id_cmd',$request->input('id'))->where('confirme',1)->orderBy('id_cmd', 'desc')->paginate(5);
               
      
   
            return view('admin.recuperercmd' , compact('cmd'));
 }



 public function listcmdpig(){ $this->policyAdmin();//list commabde 
 $cmd = DB::table('commandes')
            ->where('commandes.deleted_at',null)->orderBy('id_cmd', 'desc')->where('confirme',1)->paginate(5);
               
      
   
            return view('admin.CmdPg' , compact('cmd'));
 }



 






}
