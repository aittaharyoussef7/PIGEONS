<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPG extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [



         'nom' => 'string|max:20|min:4',
         
            'qnt' => 'integer|min:1|',
            'prix' => 'integer|min:1|',
             'description' => 'string|max:300|min:10',
   
           'photo1'=>'required|mimes:jpg,jpeg,png|max:50000',
   'photo2'=>'mimes:jpg,jpeg,png|max:50000',
              'photo3'=>'mimes:jpg,jpeg,png|max:50000',
                 'photo4'=>'mimes:jpg,jpeg,png|max:50000',

            //
        ];
    }
}
