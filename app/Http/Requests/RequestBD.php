<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestBD extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

             'date_d' =>  'required|date|after:today',
     'date_f' =>  'required|date|after:date_d',
              'nom' => 'string|max:20|min:4',

      'photo1'=>'mimes:jpg,jpeg,png|max:50000',

      
            //
        ];
    }
}
