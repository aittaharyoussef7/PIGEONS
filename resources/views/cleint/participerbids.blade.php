


@extends('layouteOrigine.menuClient')

@section('content')
	<!-- breadcrumb -->


<section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url({{asset('PhotoPigeon/p9.jpg')}});">
		<h2 class="ltext-105 cl0 txt-center">
	Pigeons
		</h2>
	</section>	



	<div class="container">
		<div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
			<a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
				ACCEUIL
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<a href="product.html" class="stext-109 cl8 hov-cl1 trans-04">
			Pigeons
				<i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
			</a>

			<span class="stext-109 cl4">
				Titre de Pigeon
			</span>
		</div>
	</div>
	



	<!-- Product Detail -->
	<section class="sec-product-detail bg0 p-t-65 p-b-60">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-7 p-b-30">
					<div class="p-l-25 p-r-30 p-lr-0-lg">
						<div class="wrap-slick3 flex-sb flex-w">
							<div class="wrap-slick3-dots"></div>
							<div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

							<div class="slick3 gallery-lb">
								<div class="item-slick3" data-thumb="{{asset('PhotoPigeon/p3.jpg')}}">
									<div class="wrap-pic-w pos-relative">
										<img src="	{{asset('PhotoPigeon/p3.jpg')}}" alt="IMG-PRODUCT">

										<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{asset('PhotoPigeon/p3.jpg')}}">
											<i class="fa fa-expand"></i>
										</a>
									</div>
								</div>

								<div class="item-slick3" data-thumb="{{asset('PhotoPigeon/p4.jpg')}}">
									<div class="wrap-pic-w pos-relative">
										<img src="{{asset('PhotoPigeon/p4.jpg')}}" alt="IMG-PRODUCT">

										<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{asset('PhotoPigeon/p4.jpg')}}">
											<i class="fa fa-expand"></i>
										</a>
									</div>
								</div>

								<div class="item-slick3" data-thumb="{{asset('PhotoPigeon/p2.jpg')}}">
									<div class="wrap-pic-w pos-relative">
										<img src="{{asset('PhotoPigeon/p2.jpg')}}" alt="IMG-PRODUCT">

										<a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{asset('PhotoPigeon/p2.jpg')}}">
											<i class="fa fa-expand"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				<div class="col-md-6 col-lg-5 p-b-30">
					<div class="p-r-50 p-t-5 p-lr-0-lg">
						<h4 class="mtext-105 cl2 js-name-detail p-b-14">
							NL.16-1148833 "Annemarie 833"
						</h4>

						<span class="mtext-106 cl2">
						700DH
						</span>

						<p class="stext-102 cl3 p-t-23">
							Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
						</p>
						
						<!--  -->
								<br>	
                         <p>   <strong><h5>Éleveur</h5></strong>    <strong><h5>Vanneste Norbert </h5></strong>  </br></p>	
                           <p>   <strong><h5>Sexe</h5></strong>    <strong><h5>Mâle </h5></strong>  	</br></p>
                            <p>  <strong><h5>Catégorie</h5></strong>    <strong><h5>Moyenne distance </h5></strong>  	</br></p>
                                	

							<div class="flex-w flex-r-m p-b-10">
								<div class="size-204 flex-w flex-m respon6-next">
								<div class="form-group">
  <label for="usr">Prix:</label>
  <input type="text" class="form-control" id="usr">
</div>

									<button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
										Valider
									</button>
								</div>
							</div>	
						</div>

						<!--  -->
				
					</div>
				</div>
			</div>

		</div>
	</section>


<h2 style="text-align: center;">tableau des participants</h2>  

<table class="table table-dark" style="margin-top: 2%;" width="500px" >
  <thead>
    <tr>
      <th scope="col" style="background-color: 	#6495ED" width="100px">N CMD</th>
      <th scope="col" style="background-color:	#6495ED" width="100px">Prix Total</th>

      <th scope="col" style="background-color: 	#6495ED" width="100px">Date</th>

    
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>223DH</td>
      <td>2018/05/05</td>
  
           
    </tr>
  
    <tr>
      <th scope="row">1</th>
      <td>223DH</td>
      <td>2018/05/05</td>
 
        
    </tr>
  
    <tr>
      <th scope="row">1</th>
      <td>223DH</td>
      <td>2018/05/05</td>
   
       
    </tr>
  
   
  
  </tbody>
</table>


		
@endsection