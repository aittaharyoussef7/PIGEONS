@extends('layouteOrigine.menuClient')

@section('content')
@foreach($bids as $bid)
    <!-- Content page -->
    <section class="bgwhite p-t-55 p-b-65 bodyimg">
        <div class="container">
            <div class="row">


                <div class="col-sm-6 col-md-4 col-lg-3 p-b-50" style="background-color: rgba(85, 221, 195, 0.4); 
">
                    <div class="leftbar p-r-20 p-r-0-sm">
                        <!--  -->
                        <h4 class="m-text14 p-b-7">
                            {{$bid->nom_bid}}
                        </h4>

                      <img src="{{asset(''.$bid->photo_bids)}}" height="400px" width="240px;">

                        <!--  -->
                        <div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
                    <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        PLUS INFORMATIONS
                        <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                        <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                    </h5>

                    <div class="dropdown-content dis-none p-t-15 p-b-23" style="word-wrap: break-word;
   width: 200px;">
              {{$bid->description}}
                    </div>
                </div  >


        <div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
                    <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        LA Duré
                        <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                        <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                    </h5>

                    <div class="dropdown-content dis-none p-t-15 p-b-23" >
               Date Début : {{$bid->date_d}} <br>
                 Date Fin  : {{$bid->date_f}}
                    </div>
              
              <?php  $pgbids = DB::table('bids_pigs')


->join('pigeons', 'pigeons.id_pig', '=', 'Bids_pigs.id_pig')

->where('Bids_pigs.id_bid',$bid->id_bid)
            ->get();?>
              
                </div>

                      

                       

                       
                    </div>
                </div>










                <div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                    <!--  -->
                

                    <!-- Product -->
                    <div class="row">



@foreach($pgbids as $pgbid)
                        <div class="col-sm-12 col-md-6 col-lg-4 p-b-50" >
                            <!-- Block2 -->
                            <div class="block2" style="background-color: #FFFFFF">
                               

                                    <?php $sups= DB::table('supportpgns')->where('id_pig',$pgbid->id_pig)->limit(1)->get();?>
@foreach($sups as $sup)

                                    <img src="{{asset(''.$sup->photo)}}" alt="IMG-PRODUCT" height="300px" width="260px">
@endforeach
                                    <div class="block2-overlay trans-0-4">
                                      

                                        <div class="block2-btn-addcart w-size1 trans-0-4">
                                            <!-- Button -->
                                            <a href=<?php echo "/infobidspg/".$pgbid->id_pig; ?>>
                                            <button style="background-color: #8FDFC9" class=" button button--ujarak button--border-thin button--text-thick">
                                              Participer
                                            </button></a>


                                        </div>
                                    </div>
                                

                                <div class="block2-txt p-t-20" style="border-top:  2px solid red">
                                    <a href="product-detail.html" class="block2-name dis-block s-text3 p-b-5">
                                      {{$pgbid->nom_pgn}}
                                    </a>

                                    <span class="block2-price m-text6 p-r-5">
                                  
                                    </span>
                                </div>
                            </div>
                        </div>
@endforeach










                    </div>

                    <!-- Pagination -->
                    
                </div>
            </div>
        </div>
    </section>
    @endforeach
@endsection