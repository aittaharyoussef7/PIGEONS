
@extends('layouteOrigine.menuClient')

@section('content')

@foreach($pros  as $pro)
    
<?php  if(!isset($_SESSION)){session_start();}
if(!isset($_SESSION['panier']['produits'])){$_SESSION['panier']['produits']=array();}
   if(!isset($_SESSION['panier']['pigeons'])){$_SESSION['panier']['pigeons']=array();} ?>

    <!-- breadcrumb -->
    <div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">
        <a href="/" class="s-text16">
            Acceuil
            <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
        </a>

        <a href="/produits" class="s-text16">
            PRODUITS
            <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
        </a>

        <a href="#" class="s-text16">
            {{$pro->nom_pro}}
            <i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>
        </a>

      
    </div>

    <!-- Product Detail -->
    <div class="container bgwhite p-t-35 p-b-80 bodyimg" >
        <div class="flex-w flex-sb">
            <div class="w-size13 p-t-30 respon5">
                <div class="wrap-slick3 flex-sb flex-w">
                    <div class="wrap-slick3-dots"></div>

                    <div class="slick3">
                        <div class="item-slick3" data-thumb="{{asset(''.$pro->photo_pro)}}">
                            <div class="wrap-pic-w">
                                 <img src="{{asset(''.$pro->photo_pro)}}" alt="IMG-PRODUCT">
                            </div>
                        </div>

                       
                    </div>
                </div>
            </div>

            <div class="w-size14 p-t-30 respon5">
                <h4 class="product-detail-name m-text16 p-b-13">
                   {{$pro->nom_pro}}
                </h4>

                <span class="m-text17">
                  @if($pro->promotion!=null)



                         <?php  $promotion=$pro->prix_pro*$pro->promotion/100;?>
                       <del> <font color="red"> {{$pro->prix_pro}} DH </font> </del>
                       {{$promotion}} DH
                          @endif </br>
                </span>

            

                <!--  -->
                <div class="p-t-33 p-b-60">
                   

                   

                    <div class="flex-r-m flex-w p-t-10">
                        <div class="w-size16 flex-m flex-w">
                          

                            <div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
                                <!-- Button -->
                                                              @if(!isset($_SESSION['panier']['produits'][$pro->id_pro]))
                         
                         <div  id="<?php echo "btnPR".$pro->id_pro;?>">               <!-- Button -->
                                        <button style="background-color: #C7C5C6" class=" button button--ujarak button--border-thin button--text-thick" onclick="add_panierPR({{$pro->id_pro}})" id="<?php echo "btnPR".$pro->id_pro;?>">
                                           Achetez
                                        </button></div>
@endif
                            </div>
                        </div>
                    </div>
                </div>

             
                <!--  -->
                <div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
                    <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                        Description
                        <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                        <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                    </h5>

                    <div class="dropdown-content dis-none p-t-15 p-b-23">
                     {{$pro->description}}
                    </div>
                </div>

           
            </div>
        </div>
    </div>

@include('cleint.welcome.welcomePR')

@include('cleint.welcome.welcomePG')

    @endforeach
@endsection

