

@extends('layouteOrigine.menuClient')

@section('content')

@foreach($pigs  as $pig)
	<form method="POST" action="/StorePar" enctype="multipart/form-data" class="contact2-form validate-form" >
			<!-- Cart item -->{{ csrf_field() }}

	<!-- breadcrumb -->


	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80 bodyimg " style="width: 400%;">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
					<div class="wrap-slick3-dots"></div>

					<div class="slick3">
<?php $sups= DB::table('supportpgns')->where('id_pig',$pig->id_pig)->get();?>
@foreach($sups as $sup)
						<div class="item-slick3" data-thumb="{{asset($sup->photo)}}">
							<div class="wrap-pic-w">
								<img src="{{asset($sup->photo)}}" alt="IMG-PRODUCT">
							</div>
						</div>

					
@endforeach
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
					{{$pig->nom_pgn}}
				</h4>

				<span class="m-text17">
					
				</span>

			

			<?php $cats= DB::table('categories')->where('id_cat',$pig->id_cat)->get();?>
					<div class="flex-m flex-w">
						<div class="s-text15 w-size15 t-center">
							Catégorie 
						</div>
                  @foreach($cats as $cat) {{$cat->nom_cat}} @endforeach
						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							
						</div>
					</div>

				<!--  -->
				<div class="p-t-33 p-b-60">
					<div class="flex-m flex-w p-b-10">
						<div class="s-text15 w-size15 t-center">
							Size
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
						{{$pig->sexe}}
						</div>
					</div>





					<div class="flex-m flex-w">
						<div class="s-text15 w-size15 t-center">
							Déstance 
						</div>

						<div class="rs2-select2 rs3-select2 bo4 of-hidden w-size16">
							{{$pig->distance}}
						</div>
					</div>


				
<input type="hidden" name="id_pig" value="{{$pig->id_pig}}">


					<div class="flex-r-m flex-w p-t-10">PRIX EN DH
						<div class="w-size16 flex-m flex-w">
							<div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
								<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
								</button>

								<input class="size8 m-text18 t-center num-product" type="number" name="prix" value="1" >

								<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
									<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>

							<div class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
								<!-- Button -->
								










@if(!Auth::user())
				<div class="size10 trans-0-4 m-t-10 m-b-10"  >
					<!-- Button -->
					
					<a class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" data-toggle="modal" data-target="#myModal">
					<font color="#FFFFF">PASSER COMMANDE</font>	
					</a>
				</div>@endif


				@if(Auth::user())
				<div class="size10 trans-0-4 m-t-10 m-b-10"  >
					<!-- Button -->
					
					<a class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" data-toggle="modal" data-target="#myModalcmd">
					<font color="#FFFFF">PASSER COMMANDE</font>	
					</a>
				</div>@endif















<div id="myModalcmd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Connecter </h4>
      </div>
      <div class="modal-body">
        <p> @include('cleint.livraison') </p>
        	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


















							</div>
						</div>
					</div>
				</div>

							




				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Description
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
					{{$pig->description}}
					</div>
				</div>


					<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Description
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">

<?php $pars= DB::table('participers')->where('id_pig',$pig->id_pig)->join('users', 'users.id' , '=' , 'participers.id_user')->get();?>


					<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">NOM</th>
      <th scope="col">PRIX</th>
     
    </tr>
  </thead>
  <tbody>@foreach($pars as $par)
    <tr>


      <th scope="row">{{$par->id_par}}</th>
      <td>{{$par->name}}</td>
      <td>{{$par->prix_par}} DH</td>
      

    </tr>
@endforeach
    <tr>
    
  </tbody>
</table>

					</div>
				</div>
				

				
			</div>
		</div>
	</div>


	<!-- Relate Product -->
@include('cleint.welcome.welcomePG')



	@endforeach


</form>
@endsection

