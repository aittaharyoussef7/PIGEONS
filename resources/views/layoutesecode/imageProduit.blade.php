

<style type="text/css">
    /* apply a natural box layout model to all elements */

.shadow {
  display: inline-block;
  position: relative;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  -moz-box-shadow: rgba(0, 0, 0, 0.8) 3px 3px 10px inset;
  -webkit-box-shadow: rgba(0, 0, 0, 0.8) 3px 3px 10px inset;
  box-shadow: rgba(0, 0, 0, 0.8) 3px 3px 10px inset;
  -webkit-transition: box-shadow 0.2s ease-in;
  -moz-transition: box-shadow 0.2s ease-in;
  transition: box-shadow 0.2s ease-in;
}
.shadow:hover {
  -moz-box-shadow: rgba(0, 0, 0, 0.8) 5px 5px 55px inset;
  -webkit-box-shadow: rgba(0, 0, 0, 0.8) 5px 5px 55px inset;
  box-shadow: rgba(0, 0, 0, 0.8) 5px 5px 55px inset;
}
.shadow img {
  max-width: 100%;
  position: relative;

  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
}

.column {
  float: left;
  width: 25%;
  padding: 0 15px;
}

/*This was all taken from https://tympanus.net/Development/ButtonStylesInspiration/ but I needed to see it in CodePen
*/
@import url(https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600);
@font-face {
  font-weight: normal;
  font-style: normal;
  font-family: 'codropsicons';
  src: url("../fonts/codropsicons/codropsicons.eot");
  src: url("../fonts/codropsicons/codropsicons.eot?#iefix") format("embedded-opentype"), url("../fonts/codropsicons/codropsicons.woff") format("woff"), url("../fonts/codropsicons/codropsicons.ttf") format("truetype"), url("../fonts/codropsicons/codropsicons.svg#codropsicons") format("svg");
}
*,
*:after,
*:before {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
.cf:before,
.cf:after {
  content: '';
  display: table;
}
.cf:after {
  clear: both;
}
body {
  background: #cfd8dc;
  color: #37474f;
  font-weight: 400;
  font-size: 1em;
  font-family: 'Raleway', Arial, sans-serif;
}
.support {
  font-weight: bold;
  padding: 2em 0 0 0;
  font-size: 1.4em;
  color: #ee2563;
  display: none;
}
a {
  color: #7986cb;
  text-decoration: none;
  outline: none;
}
a:hover,
a:focus {
  color: #3f51b5;
}
.hidden {
  position: absolute;
  width: 0;
  height: 0;
  overflow: hidden;
  pointer-events: none;
}
.container {
  margin: 0 auto;
  text-align: center;
  overflow: hidden;
}
.content {
  padding: 2em 1em 5em;
  z-index: 1;
  max-width: 1000px;
  margin: 0 auto;
}
.content h2 {
  margin: 0 0 2em;
}
.content p {
  margin: 1em 0;
  padding: 0 0 2em;
  font-size: 0.85em;
}
.box {
  padding: 4.5em 0;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-justify-content: center;
  justify-content: center;
}

/* Header */
.codrops-header {
  padding: 3em 190px 4em;
  letter-spacing: -1px;
}
.codrops-header h1 {
  font-weight: 200;
  font-size: 4em;
  line-height: 1;
  margin-bottom: 0;
}
.codrops-header h1 span {
  display: block;
  font-size: 40%;
  letter-spacing: 0;
  padding: 0.5em 0 1em 0;
  color: #A8B3B8;
}

/* Top Navigation Style */
.codrops-links {
  position: relative;
  display: inline-block;
  white-space: nowrap;
  font-size: 1.25em;
  text-align: center;
}
.codrops-links::after {
  position: absolute;
  top: 0;
  left: 50%;
  width: 1px;
  height: 100%;
  background: #BFCACF;
  content: '';
  -webkit-transform: rotate3d(0, 0, 1, 22.5deg);
  transform: rotate3d(0, 0, 1, 22.5deg);
}
.codrops-icon {
  display: inline-block;
  margin: 0.5em;
  padding: 0em 0;
  width: 1.5em;
  text-decoration: none;
}
.codrops-icon:before {
  margin: 0 5px;
  text-transform: none;
  font-weight: normal;
  font-style: normal;
  font-variant: normal;
  font-family: 'codropsicons';
  line-height: 1;
  speak: none;
  -webkit-font-smoothing: antialiased;
}
.codrops-icon span {
  display: none;
}
.codrops-icon--drop:before {
  content: "\e001";
}
.codrops-icon--prev:before {
  content: "\e004";
}

/* Related demos */
.content--related {
  text-align: center;
  font-weight: 600;
}
.media-item {
  display: inline-block;
  padding: 1em;
  margin: 1em 0 0 0;
  vertical-align: top;
  -webkit-transition: color 0.3s;
  transition: color 0.3s;
}
.media-item__img {
  opacity: 0.8;
  max-width: 100%;
  -webkit-transition: opacity 0.3s;
  transition: opacity 0.3s;
}
.media-item:hover .media-item__img,
.media-item:focus .media-item__img {
  opacity: 1;
}
.media-item__title {
  font-size: 0.85em;
  margin: 0;
  padding: 0.5em;
}
@media screen and (max-width:50em) {
  .codrops-header {
    padding: 3em 10% 4em;
  }
}
@media screen and (max-width:40em) {
  .codrops-header h1 {
    font-size: 2.8em;
  }
}

/* Box colors */
.bg-1 {
  background: #ECEFF1;
  color: #37474f;
}
.bg-2 {
  background: #7986cb;
  color: #ECEFF1;
}
.bg-3 {
  background: #37474f;
  color: #fff;
}

/* Common button styles */
.button {


  display: block;
  margin: 1em;
  padding: 1em 2em;
  border: none;
  background: none;
  color: inherit;
  vertical-align: middle;
  position: relative;
  z-index: 1;
  -webkit-backface-visibility: hidden;
  -moz-osx-font-smoothing: grayscale;
}
.button:focus {
  outline: none;
}
.button > span {
  vertical-align: middle;
}

/* Text color adjustments (we could stick to the "inherit" but that does not work well in Safari) */
.bg-1 .button {
  color: #37474f;
  border-color: #37474f;
}
.bg-2 .button {
  color: #ECEFF1;
  border-color: #ECEFF1;
}
.bg-3 .button {
  color: #fff;
  border-color: #fff;
}


/* Borders */
.button--border-thin {
  border: 1px solid;
}
/* Individual button styles */



/* Ujarak */
.button--ujarak {
  -webkit-transition: border-color 0.4s, color 0.4s;
  transition: border-color 0.4s, color 0.4s;
}
.button--ujarak::before {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: #37474f;
  z-index: -1;
  opacity: 0;
  -webkit-transform: scale3d(0.7, 1, 1);
  transform: scale3d(0.7, 1, 1);
  -webkit-transition: -webkit-transform 0.4s, opacity 0.4s;
  transition: transform 0.4s, opacity 0.4s;
  -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
  transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
}
.button--ujarak::before {
  border-radius: 2px;
}
.button--ujarak::before {
  background: #21A4EA;
}
.button--ujarak,
.button--ujarak::before {
  -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
  transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
}

.button--text-thick {
  font-weight: 600;
}
.button:focus {
  outline: none;
}
.button--ujarak:hover {
  color: #fff;
  border-color: #21A4EA;
}
.button--ujarak:hover {
  color: #37474F;
  border-color: #21A4EA;
}
.button--ujarak:hover::before {
  opacity: 1;
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0);
}








</style>