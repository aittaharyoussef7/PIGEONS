  <!-- Navbar --><?php $colornavbar =DB::table('visiteur')->get();?>
  @foreach($colornavbar as $color)
  <nav class="main-header navbar navbar-expand  navbar-light border-bottom" style="<?php echo "background-color:".$color->colorN ;?>"  > @endforeach
    <!-- Left navbar links -->
    <ul class="navbar-nav" >
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Mon site</a>
      </li>
  
    </ul>

    <!-- SEARCH FORM -->
  
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto" style="">
      <!-- Messages Dropdown Menu --> 
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-comments-o"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('PhotoPigeon/logo.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fa fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell-o"></i>
          <span class="badge badge-warning navbar-badge" id="totalAnnonces">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"  id="totalAnnonces" > Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="/CmdPg" class="dropdown-item">
            <i class="fa fa-envelope mr-2"></i> LES commandes
            <?php $nbrCs= DB::table('commandes')->select(DB::raw('count(*) as nbrC'))->where('deleted_at',null)->where('notification',0)->where('confirme',1)->get();?>
            <span class="float-right text-muted text-sm">@foreach($nbrCs as $nbrC)  {{$a=$nbrC->nbrC}} @endforeach</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="/NPG" class="dropdown-item"><?php $nbrPGs= DB::table('Pigeons')->select(DB::raw('count(*) as nbrPG'))->where('deleted_at',null)->where('notification',0)->where('bid',0)->where('status',0)->get();?>
            <i class="fa fa-users mr-2"></i> Pigeon En attent
            <span class="float-right text-muted text-sm">@foreach($nbrPGs as $nbrC)  {{$b=$nbrC->nbrPG}} @endforeach</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="/NPR" class="dropdown-item"><?php $nbrPRs= DB::table('Produits')->select(DB::raw('count(*) as nbrPR'))->where('deleted_at',null)->where('notification',0)->where('status',0)->get();?>
            <i class="fa fa-file mr-2"></i> Produits En attent
            <span class="float-right text-muted text-sm">@foreach($nbrPRs as $nbrPR)  {{$c=$nbrPR->nbrPR}} @endforeach</span>
          </a>

           <div class="dropdown-divider"></div>
          <a href="/Nbids" class="dropdown-item"><?php $nbrBDs= DB::table('bids')->select(DB::raw('count(*) as nbrBD'))->where('deleted_at',null)->where('notification',0)->where('status',0)->get();?>
            <i class="fa fa-file mr-2"></i> Enchéres En attent
            <span class="float-right text-muted text-sm">@foreach($nbrBDs as $nbrBD)  {{$d=$nbrBD->nbrBD}} @endforeach</span>
          </a>

          <div class="dropdown-divider"></div>
         <script type="text/javascript">
           document.getElementById("totalAnnonces").innerHTML = "{{$a+$b+$c+$d}}";
         </script>
        </div>
      </li>
     
    </ul>
  </nav>