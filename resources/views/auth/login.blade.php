<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="stylish Sign in and Sign up Form A Flat Responsive widget, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--online_fonts-->
    <link href="//fonts.googleapis.com/css?family=Sansita:400,400i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
<!--//online_fonts-->
    <link href="{{asset('TPigeons/login/login/css/style.css')}}" rel='stylesheet' type='text/css' media="all" /><!--stylesheet-->
</head>





<div class="form-w3ls" style="width: 100%;">
    <div class="form-head-w3l">
    
    </div> 
    <ul class="tab-group cl-effect-4">
        <li class="tab active"><a href="#signin-agile">connexion</a></li>
        <li class="tab"><a href="#signup-agile">inscription</a></li>        
    </ul>
    <div class="tab-content">
        <div id="signin-agile">   
                 <form method="POST" action="{{ route('login') }}">
                @csrf
               <p class="header">Email</p>
                <input  type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


                <p class="header">Mote de passe</p>
                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              
         
                
                <input type="submit" class="sign-in" value="connexion">
            </form>
        </div>
        <div id="signup-agile">   
                     <form method="POST" action="{{ route('register') }}">
                           @csrf


                <p class="header">Nom Utilisateur</p>
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                
                <p class="header">Email </p>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


                <p class="header">Mote de passe</p>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                
                <p class="header">Confirmer Mote de passe</p>
               <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                

                <input type="submit" class="register" value="inscription">
            </form>
        </div> 
    </div><!-- tab-content -->
</div> <!-- /form -->     

<!-- js files -->
<script src="{{asset('TPigeons/login/login/js/jquery.min.js')}}"></script>
<script src="{{asset('TPigeons/login/login/js/index.js')}}"></script>
<!-- /js files -->
