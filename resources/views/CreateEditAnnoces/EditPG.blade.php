@extends('layouteOrigine.menuClient')

@section('content')

<title>Poster Votre Annonce de Pigeon</title>
<!-- metatags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sports Camp Registration Form a Flat Responsive Widget,Login form widgets, Sign up Web 	forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('TPigeons/createEditAnnonce/asset/css/jquery-ui.css')}}"/>
<link href="{{asset('TPigeons/createEditAnnonce/asset/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/><!--stylesheet-css-->
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">




	  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>

<style type="text/css">
	.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}
</style>
<!-- //css files -->

</head>
<body style="background-image: url({{asset('PhotoPigeon/p10.jpg')}});">
<h1><font  color="#F2824A" >Modifier Votre Annonce de Pigeon</font></h1>
<div class="w3l-main">
	<div class="w3l-from">@foreach($pgs as $pg)
		<form method="POST" action=<?php echo "/UpdatePG/".$pg->id_pig;?> enctype="multipart/form-data" class="contact2-form validate-form" >
                           

    @csrf   
			<div class="w3l-user">
				<label class="head">NOM pigeon <span class="w3l-star"> * </span></label>
				<input type="text" name="nom" placeholder="" required="" style="opacity: 0.5;" value="{{$pg->nom_pgn}}" maxlength="15"
				minlength="4">
			</div>
		


			<div class="w3l-options2" style="margin-left: 4%;" >
				<label class="head" >Quntité de pigeon<span class="w3l-star"> * </span></label>
				<input type="number" name="qnt" placeholder="" required=""  class="form-control" value="{{$pg->qnt_pgn}}" style="opacity: 0.5;" max="100000">
			</div>


	<div class="w3l-options2">
			<label class="head">Catégorie<span class="w3l-star"> * </span></label>	
				<select class="category2" required="" name="id_cat"  style="opacity: 0.5;">

<?php

                 $cats = DB::table('categories')
            ->where('id_cat',$pg->id_cat)->get();
?>


  @foreach($cats as $vars)
						
		
<option value="{{$vars->id_cat}}">{{$vars->nom_cat}}</option>
			@endforeach



					<?php

                 $cat = DB::table('categories')
            ->where('deleted_at',null)->get();
?>
                      @foreach($cat as $var)
						<option value="{{$var->id_cat}}">{{$var->nom_cat}}</option>
					@endforeach
					
					</select>
			</div>

			<div class="clear"></div>
			<div class="w3l-details1">
			
				
			<div class="clear"></div>
			<div class="w3l-options2">
				<label class="head">Catégorie<span class="w3l-star"> * </span></label>	
					<select class="category2" required="" name="dis"  style="opacity: 0.5;"> 
						<option value="long destance">long destance</option>
						<option value="court destance">court destance</option>
						<option value="moyenne destance">moyenne destance </option>
					</select>
			</div>
			<div class="w3l-options2" style="margin-right: 4%;">
			<label class="head">Sexe<span class="w3l-star"> * </span></label>	
				<select class="category2" required="" name="sexe"  style="opacity: 0.5;">
						<option value="{{$pg->sexe}}">{{$pg->sexe}}</option>
						<option value="MALE">MALE</option>
						<option value="Femme">Femme</option>
					
					</select>
			</div>
	
<?php $i=0;?>
@isset($id_bid)
<input  name="id_bid" type="hidden" value="{{$id_bid}}">
<?php $i=1;?>
@endisset

@if($i==0)
			<div class="clear">	<div class="w3l-user">
				<label class="head">Prix DH<span class="w3l-star" > * </span></label>
				<input type="number" name="prix" placeholder="" required="" class="form-control"  style="opacity: 0.5;" value="{{$pg->prix}}" max="100000">
			</div></div>
		
	@if($pg->promotion==100)
	<?php $promo=0;?>
	@else 
	<?php $promo=$pg->promotion;?>
@endif
				<div class="clear">	<div class="w3l-user">
				<label class="head">Promotion% <span class="w3l-star" >  </span></label>
 
				<input type="number" name="promotion" placeholder=""  class="form-control"  style="opacity: 0.5;" value="{{$promo}}" max="100" min="0">
			</div></div>
			</div>

@endif

				<div class="w3l-rem">
				<div class="w3l-right">
					<label class="w3l-set2">Description </label>
					<textarea name="des"  style="opacity: 0.5;">{{$pg->description}}</textarea>
				</div>	
			
			</div>



					<?php

                 $sups = DB::table('supportpgns')
            ->where('id_pig',$pg->id_pig)->get();
?>
			<div class="w3l-user">
				@foreach($sups as $sup)
				<label class="head">photo<span class="w3l-star"> * </span></label>
			<label class="fileContainer">

				<img src="{{asset(''.$sup->photo)}}"  style="width: 400px;height: 150px;">
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;">
    <input type="file" name= "<?php echo "".$sup->id_sup;?>">
</label>

@endforeach



	
	
		
			@isset($id_bid)
					<input type="submit" name="submit" value="Terminer"/ style="background-color: red;">
@endisset
</div>
@if(Auth::User()->admin==1)
@if($i==0)

			 <div class="radio">
			 			<label class="head">Affichier à page index<span class="w3l-star"> * </span></label>	
      <label><input type="radio" name="welcome" value="1"><label class="head">OUI<span class="w3l-star"></label>
    </div>
    <div class="radio">
      <label><input type="radio" name="welcome" value="0"><label class="head">NON<span class="w3l-star"></label></label>
    </div>

@endif @endif
			<div class="btn btn-outline-secondary" style="margin-left: 30%;">
					<input type="submit" name="submit" value="Valider"/ onclick="valider()" style="background-color: #90F3C4;width: 200px; height: 40px;">
	</div>
			@endforeach
		</form>
	</div>
</div>
	<footer>
	</footer>
	<!-- Default-JavaScript --> <script type="text/javascript" src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-2.1.4.min.js')}}"></script>

<!-- Calendar -->
<script src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-ui.js')}}"></script>
	<script>
		$(function() {
		$( "#datepicker,#datepicker1" ).datepicker();
		});
	</script>

</span></label></label></div></span></label></label></div></form></div></div></body>
@endsection