

@extends('layouteOrigine.menuAdmin')

@section('content')
<!-- metatags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sports Camp Registration Form a Flat Responsive Widget,Login form widgets, Sign up Web 	forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('TPigeons/createEditAnnonce/asset/css/jquery-ui.css')}}"/>
<link href="{{asset('TPigeons/createEditAnnonce/asset/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/><!--stylesheet-css-->
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">




	  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>

<style type="text/css">
	.fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}
</style>
<!-- //css files -->

</head>
  <!-- Navbar --><?php $colornavbar =DB::table('visiteur')->get();?>
  @foreach($colornavbar as $color)

<body >
		@if(session()->get('sucBDPG'))

<script type="text/javascript">

swal(
  'Opération a été couronnée de succès !',
  '!',
  'success'
)
</script>

@endif


	<h1>Poster Votre Annonce de Pigeon</h1>
<div class="w3l-main" style="<?php echo "background-color:".$color->colorN ;?>"  >  @endforeach

	<div class="w3l-from">
				<form method="POST" action="/StorePG" enctype="multipart/form-data" class="contact2-form validate-form" >
    @csrf
		
			<div class="w3l-user">
				<label class="head">NOM pigeon <span class="w3l-star" > * </span></label>
				<input type="text" name="nom" placeholder="" required="" style="opacity: 0.5;" value="{{old('nom')}}" maxlength="15" minlength="4">
			</div>


   @if ($errors->has('nom'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('nom') }}</font>    
                                    </span>
                                @endif






			<div class="w3l-options2" style="margin-left: 4%;" >
				<label class="head" >Quntité de Pigeon<span class="w3l-star"> * </span></label>
				<input type="number" name="qnt" placeholder="" required=""  class="form-control"  style="opacity: 0.5;" value="{{old('qnt')}}" min="1" max="100000" >




			</div>

     @if ($errors->has('qnt'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('qnt') }}</font>    
                                    </span>
                                @endif
                                








			
			<div class="w3l-details1">
					<div class="w3l-options2">
			<label class="head">Catégorie<span class="w3l-star"> * </span></label>	
				<select class="category2" required="" name="id_cat"  style="opacity: 0.5;">

					<?php

                 $cat = DB::table('categories')
            ->where('deleted_at',null)->get();
?>
                      @foreach($cat as $var)
						<option value="{{$var->id_cat}}">{{$var->nom_cat}}</option>
					@endforeach
					
					</select>
			</div>




				
			


	
<?php $i=0;?>
@isset($id_bid)
<input  name="id_bid" type="hidden" value="{{$id_bid}}">
<?php $i=1;?>
@endisset

@if($i==0)
			<div class="clear">	<div class="w3l-user">
				<label class="head">Prix DH<span class="w3l-star" > * </span></label>
				<input type="number" name="prix" placeholder="" required="" class="form-control" min="1" style="opacity: 0.5;" value="{{old('prix')}}" max="100000" min="1">
			</div></div>
		 @if ($errors->has('prix'))
                                    <span class="help-block">
                                  <font color=#DC143C></font>    
                                    </span>
                                @endif
			


				<div class="clear">	<div class="w3l-user">
				<label class="head">Promotion % <span class="w3l-star">  </span></label>
				<input type="number" name="promotion" placeholder="" class="form-control" max="100" style="opacity: 0.5;"  value="{{old('promotion')}}" min="1">
			</div></div>
		

@endif


<div class="w3l-options2">
				<label class="head">Catégorie<span class="w3l-star"> * </span></label>	
					<select class="category2" required="" name="dis"  style="opacity: 0.5;"> 
						<option value="long destance">long destance</option>
						<option value="court destance">court destance</option>
						<option value="moyenne destance">moyenne destance </option>
					</select>
			</div>
			<div class="w3l-options2" style="margin-right: 4%;">
			<label class="head">Sexe<span class="w3l-star"> * </span></label>	
				<select class="category2" required="" name="sexe"  style="opacity: 0.5;">
						<option value="MALE">MALE</option>
						<option value="Femme">Femme</option>
					
					</select>
			</div>

		





			<div class="clear"></div>
				<div class="w3l-right">
					<label class="w3l-set2">Description </label>
					<textarea name="des"  style="opacity: 0.5;"  minlength="10" maxlength="1000">{{old('des')}}</textarea>
				</div>	


			
			<div class="clear"></div>
			</div>

				<label class="head">photo<span class="w3l-star"> * </span></label>
			<label class="fileContainer">
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;" >
    <input type="file" name="photo1" required="">
</label>
	     @if ($errors->has('photo1'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('photo1') }}</font>    
                                    </span>
                                @endif

			<label class="fileContainer">
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;">
    <input type="file" name="photo2" >
</label>

 @if ($errors->has('photo2'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('photo2') }}</font>    
                                    </span>
                                @endif



			<label class="fileContainer">
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;">
    <input type="file" name="photo3">
</label>

 @if ($errors->has('photo3'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('photo3') }}</font>    
                                    </span>
                                @endif



			<label class="fileContainer">
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;">
    <input type="file" name="photo4">
</label>

 @if ($errors->has('photo4'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('photo4') }}</font>    
                                    </span>
                                @endif



			

	
	
@if(Auth::User()->admin==1)

@if($i==0)

           <div class="radio">
                        <label class="head">Affichier à page index<span class="w3l-star"> * </span></label> 
      <label><input type="radio" name="welcome" value="1" required=""><label class="head">OUI<span class="w3l-star"></label>
    </div>
    <div class="radio">
      <label><input type="radio" name="welcome" value="0" required=""><label class="head">NON<span class="w3l-star"></label></label>
    </div>


    		<div class="gender btnx " style="margin-top: 4%;" >
		
</div>
@endif        @endif

	@isset($id_bid)
				<div class="btn btn-outline-secondary" style="margin-left: 30%;">
					<input type="submit" name="terminer" value="Valider"/ onclick="Terminer()" style="background-color:#F6CEE3;width: 200px; height: 40px;">
				</div>	

@endisset
				<div class="btn btn-outline-secondary" style="margin-left: 30%;">
					<input type="submit" name="submit" value="Ajouter "/ onclick="valider()" style="background-color: #90F3C4;width: 200px; height: 40px;">
				</div>





			
		
		
			
			</div>
			<div class="clear"></div>
		</span></label></label></div></span></label></label></div></form>
	</div>
</div>
	<footer>
	</footer>
	 <script type="text/javascript" src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-2.1.4.min.js')}}"></script>

<!-- Calendar -->
<script src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-ui.js')}}"></script>
	<script>
		$(function() {
		$( "#datepicker,#datepicker1" ).datepicker();
		});
	</script>

</body>
</html>   @endsection

