
@extends('layouteOrigine.menuAdmin')

@section('content')

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sports Camp Registration Form a Flat Responsive Widget,Login form widgets, Sign up Web   forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('TPigeons/createEditAnnonce/asset/css/jquery-ui.css')}}"/>
<link href="{{asset('TPigeons/createEditAnnonce/asset/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/><!--stylesheet-css-->
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">




      <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  

<style type="text/css">
    .fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}
</style>
<!-- //css files -->

</head>


<body >
    <h1>Poster Votre Annonce de Pigeon</h1>
<?php $colornavbar =DB::table('visiteur')->get();?>
  @foreach($colornavbar as $color)
<div class="w3l-main" style="<?php echo "background-color:".$color->colorN ;?>"  >  @endforeach
    <div class="w3l-from" >@foreach($bids as $bid)
        <form method="POST" action=<?php echo "/UpdateBD/".$bid->id_bid;?> enctype="multipart/form-data" class="contact2-form validate-form" >
                           

    @csrf
            <div class="w3l-user">
                <label class="head">Titre de bids<span class="w3l-star"> * </span></label>
                <input type="text" name="nom" placeholder="" required=""  style="opacity: 0.5;" value="{{$bid->nom_bid}}">
            </div>
     

            <div class="w3l-options2" style="margin-left: 4%;" >
                <label class="head" >Date Fin<span class="w3l-star"> * </span></label>
                <input type="date" name="date_f" placeholder="" required=""  class="form-control"  style="opacity: 0.5;" value="{{$bid->date_f}}">
            </div>


    <div class="w3l-options2">
            <label class="head">Date Début<span class="w3l-star"> * </span></label>    
            <input type="date" name="date_d" placeholder="" required=""  class="form-control"  style="opacity: 0.5;" value="{{$bid->date_d}}" >
            </div>

            <div class="clear"></div>
            <div class="w3l-details1">
            
            

                <div class="w3l-rem">
                <div class="w3l-right">
                    <label class="w3l-set2">Description </label>
                    <textarea name="des"  style="opacity: 0.5;">{{$bid->description}}</textarea>
                </div>  
            
            </div>

            <div class="w3l-user" style="margin-top: 5%;">
                <label class="head">photo</br><span class="w3l-star"> * </span></label>
            <label class="fileContainer">
                 <img src="{{asset(''.$bid->photo_bids)}}"  style="width: 400px;" > 
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;">
    <input type="file" name="photo1">
</label>



            </div>  
@if(Auth::User()->admin==1)
             <div class="radio">
                        <label class="head">Affichier à page index<span class="w3l-star"> * </span></label> 
      <label><input type="radio" name="welcome" value="1"><label class="head">OUI<span class="w3l-star"></label>
    </div>
    <div class="radio">
      <label><input type="radio" name="welcome" value="0"><label class="head">NON<span class="w3l-star"></label></label>
    </div> @endif<div class="btn btn-outline-secondary" style="margin-left: 30%;">
                    <input type="submit" name="submit" value="Valider"/ onclick="valider()" style="background-color: #90F3C4;width: 200px; height: 40px;">
                </div>
</div>
  
    
            <div class="clear"></div>
            </div>  
              
            <div class="clear"></div>
        </form>@endforeach
    </div>
</div>
    <footer>
    </footer>
    <!-- Default-JavaScript --> <script type="text/javascript" src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-2.1.4.min.js')}}"></script>

<!-- Calendar -->
<script src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-ui.js')}}"></script>
    <script>
        $(function() {
        $( "#datepicker,#datepicker1" ).datepicker();
        });
    </script>
</span></label></label></div></span></label></label></div></div></form></div></div></body>
@endsection