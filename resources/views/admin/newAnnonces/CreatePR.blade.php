
@extends('layouteOrigine.menuAdmin')

@section('content')

<html>
<!-- metatags-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sports Camp Registration Form a Flat Responsive Widget,Login form widgets, Sign up Web   forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Meta tag Keywords -->
<!-- css files -->
<link rel="stylesheet" href="{{asset('TPigeons/createEditAnnonce/asset/css/jquery-ui.css')}}"/>
<link href="{{asset('TPigeons/createEditAnnonce/asset/css/style.css')}}" rel="stylesheet" type="text/css" media="all"/><!--stylesheet-css-->
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">




    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  

<style type="text/css">
  .fileContainer {
    overflow: hidden;
    position: relative;
}

.fileContainer [type=file] {
    cursor: inherit;
    display: block;
    font-size: 999px;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
}
</style>
<!-- //css files -->

</head>


<body >
  <h1>Poster Votre Annonce de Produit</h1>
  <?php $colornavbar =DB::table('visiteur')->get();?>
  @foreach($colornavbar as $color)
<div class="w3l-main" style="<?php echo "background-color:".$color->colorN ;?>"  >  @endforeach
  <div class="w3l-from">
    <form method="POST" action="/StorePR" enctype="multipart/form-data" class="contact2-form validate-form" >
         @csrf

  <div class="w3l-user">
        <label class="head">NOM Produit<span class="w3l-star"> * </span></label>
        <input type="text" name="nom" placeholder="" required="" style="opacity: 0.5;" value="{{old('nom')}}">
           @if ($errors->has('nom'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('nom') }}</font>    
                                    </span>
                                @endif
      </div>
    



      <div class="w3l-options2" style="margin-left: 4%;" >
        <label class="head" >Prix de pigeon<span class="w3l-star"> * </span></label>
        <input type="number" name="prix" placeholder="" required=""  class="form-control" style="opacity: 0.5;" value="{{old('prix')}}">
           @if ($errors->has('prix'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('prix') }}</font>    
                                    </span>
                                @endif
      </div>








    


    


        <div class="w3l-options2">
      <label class="head">Quntité<span class="w3l-star"> * </span></label>  
      <input type="number" name="qnt" placeholder="" required=""  class="form-control" style="opacity: 0.5;" value="{{old('qnt')}}">
         @if ($errors->has('qnt'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('qnt') }}</font>    
                                    </span>
                                @endif

      </div>










<div class="clear"> <div class="w3l-user">
        <label class="head">Promotion% <span class="w3l-star" >  </span></label>
        <input type="number" name="promotion" placeholder=""  class="form-control" max="100" style="opacity: 0.5;" value="{{old('promotion')}}">
      </div></div>








      <div class="clear"></div>
    <div class="w3l-right">
          <label class="w3l-set2">Description </label>
          <textarea name="des" style="opacity: 0.5;"> {{old('des')}}</textarea>
        </div>


    
        <div class="w3l-user">
        <label class="head">photo<span class="w3l-star"> * </span></label>
      <label class="fileContainer">
           @if ($errors->has('photo1'))
                                    <span class="help-block">
                                  <font color=#DC143C>{{ $errors->first('photo1') }}</font>    
                                    </span>
                                @endif
  <img src="{{asset('TPigeons/createEditAnnonce/asset/1.png')}}"  style="width: 70px;" style="opacity: 0.5;">
    <input type="file" name="photo1">
</label>



      </div>




  @if(Auth::user()->admin==1)

       <div class="radio">
            <label class="head">Affichier à page index<span class="w3l-star"> * </span></label> 
      <label><input type="radio" name="welcome" value="1" required=""><label class="head">OUI<span class="w3l-star"></label>
    </div>
    <div class="radio">
      <label><input type="radio" name="welcome" value="0" required=""><label class="head">NON<span class="w3l-star"></label></label>
    </div>
@endif






       <div class="btn btn-outline-secondary" style="margin-left: 30%;">
          <input type="submit" name="submit" value="Valider"/ onclick="valider()" style="background-color: #90F3C4;width: 200px; height: 40px;">
        </div>
      
    
    </form>
  </div>
</div>
  <footer>
  </footer>
  <!-- Default-JavaScript --> <script type="text/javascript" src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-2.1.4.min.js')}}"></script>

<!-- Calendar -->
<script src="{{asset('TPigeons/createEditAnnonce/asset/js/jquery-ui.js')}}"></script>
  <script>
    $(function() {
    $( "#datepicker,#datepicker1" ).datepicker();
    });
  </script>
<!-- //Calendar -->

</body>
</html>
@endsection