<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

@extends('layouteOrigine.menuAdmin')

@section('content')
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="stylish Sign in and Sign up Form A Flat Responsive widget, Login form web template,Flat Pricing tables,Flat Drop downs  Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--online_fonts-->
    <link href="//fonts.googleapis.com/css?family=Sansita:400,400i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
<!--//online_fonts-->
    <link href="{{asset('TPigeons/login/login/css/style.css')}}" rel='stylesheet' type='text/css' media="all" /><!--stylesheet-->
</head>





<div class="form-w3ls" style="width:50%; ">
    <div class="form-head-w3l">
    
    </div> 
    <ul class="tab-group cl-effect-4">
        <li class="tab active"><a href="#signin-agile">MOdifier Votre Compte</a></li>
      
    </ul>
    <div class="tab-content">
        <div id="signin-agile">   
                 <form method="POST" action="MOdifierLOGIN">
                @csrf
               <p class="header">NOM</p>
                <input  type="text"  name="name" required value="{{Auth::user()->name}}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


  <p class="header">Email</p>
                <input  type="text"  name="email" required value="" placeholder="{{Auth::user()->email}}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif




                <p class="header">Mote de passe</p>
                   <input  type="password"  name="password" required placeholder="****************">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
              
         
                
                <input type="submit" class="sign-in" value="connexion">
            </form>
        </div>
        <div id="signup-agile">   
     
        </div> 
    </div><!-- tab-content -->
</div> <!-- /form -->     

<!-- js files -->
<script src="{{asset('TPigeons/login/login/js/jquery.min.js')}}"></script>
<script src="{{asset('TPigeons/login/login/js/index.js')}}"></script>
<!-- /js files -->
@endsection