<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Baxster an Admin Panel Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Baxster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>





<link rel="stylesheet" type="text/css" href="{{asset('TPigeons/assets/css/bootstrap.min.css')}}">
<script src="{{asset('TPigeons/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('TPigeons/assets/js/jquery.min.js')}}"></script>

<style type="text/css">
	 table{border:2px solid black;border-style:outset;text-align:center;}
 table tr th{background-color:#A4A4A4;border:0.5px solid  darkgrey;color:floralwhite;text-align:center;}
 table tr td{font-size:15px;}

</style>



<!-- Custom CSS -->
<link href="{{asset('TPigeons/admin/layoute/css/style.css')}}" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="icon" href="favicon.ico" type="image/x-icon" >
<!-- font-awesome icons -->
<link href="{{asset('TPigeons/admin/layoute/css/font-awesome.css')}}" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- chart -->
<script src="{{asset('TPigeons/admin/layoute/js/Chart.js')}}"></script>
<!-- //chart -->
 <!-- js-->
<script src="{{asset('TPigeons/admin/layoute/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('TPigeons/admin/layoute/js/modernizr.custom.js')}}"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts--> 
<!--animate-->
<link href="{{asset('TPigeons/admin/layoute/css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
<script src="{{asset('TPigeons/admin/layoute/js/wow.min.js')}}"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="{{asset('TPigeons/admin/layoute/js/metisMenu.min.js')}}"></script>
<script src="{{asset('TPigeons/admin/layoute/js/custom.js')}}"></script>
<link href="{{asset('TPigeons/admin/layoute/css/custom.css')}}" rel="stylesheet">
<!--//Metis Menu -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<div class="sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right dev-page-sidebar mCustomScrollbar _mCS_1 mCS-autoHide mCS_no_scrollbar" id="cbp-spmenu-s1">
					<div class="scrollbar scrollbar1">
						<ul class="nav" id="side-menu">
							<li>
								<a href="/admin" class="active"><i class="fa fa-home nav_icon"></i>ACCEUIL</a>
							</li>
						
							<li>
								<a href="/CmdPg"><i class="fa fa-book nav_icon"></i>Les Commandes <span class="fa arrow"></span></a>
							av-second-level -->
							</li>
				<li>
								<a href="#"><i class="fa fa-th-large nav_icon" ></i>Les Annonces <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
								<a href="#"><i class="fa fa-th-large nav_icon" ></i> ANNONCES N'EST PAS AUTORISER  <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<li>
										<a href="/NPG">Pigeons</a>
									</li>
									<li>
										<a href="/NPR">Produits</a>
									</li>


									<li>
										<a href="/Nbids">Bids</a>
									</li>


								</ul>
								<!-- /nav-second-level -->
							</li>


								<li>
								<a href="#"><i class="fa fa-th-large nav_icon" ></i> ANNONCES  AUTORISER  <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<li>
										<a href="APG">Pigeons</a>
									</li>
									<li>
										<a href="APR">Produits</a>
									</li>


									<li>
										<a href="/Abids">Bids</a>
									</li>


								</ul>
								<!-- /nav-second-level -->
							</li>


								<!-- /nav-second-level -->
							</li>

							</ul></li>



	                         <li>
								<a href="index.html" class="active"><i class="fa fa-envelope nav_icon"></i> LES MESSAGE </a>
							</li>
      
						







			<li>
								<a href="#"><i class="fa fa-file-text-o nav_icon" ></i>Créer Annonces <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
								<a href="#"><i class="fa fa-file-text-o nav_icon" ></i> Créer une Annonces  <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<li>
										<a href="/CreatePG">Pigeons</a>
									</li>
									<li>
										<a href="/CreatePR">Produits</a>
									</li>


									<li>
										<a href="/CreateBD">Bids</a>
									</li>


								</ul>
								<!-- /nav-second-level -->
							</li>



	<li>
								<a href="/categorie"><i class="fa fa-th-large nav_icon" ></i> Catégorie pigeons  <span class="fa arrow"></span></a>
								
								<!-- /nav-second-level -->
							</li>
								<!-- /nav-second-level -->
							</li></ul>












@if(Auth::user()->sup_Admin==1)


	<li>
								<a href="/setting"><i class="fa fa-th-large nav_icon" ></i>Setting  <span class="chart-nav"></span></a></li>
							
@endif
   
   <script type="text/javascript">

function authclient($id) {


  swal({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {

  	  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("auth").innerHTML = this.responseText;
    }
  };




     xhttp.open("GET", "/authclient/"+$id, true);

  xhttp.send();



    swal(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
  }
})


}

   </script>
</li>
			

	<li>
									

											 <form id="logout-form" action="{{ route('logout') }}"  method="POST" >
                                        @csrf

                                        <button class="btn-info" type="submit"><img src="{{asset('PhotoPigeon/logout.gif')}}" height="40px" width="40px">Déconnection </button>
                                    </form>
										
                                
									</li>
									
								</ul>
								<!-- /nav-second-level -->
							</li>
						



						</ul>
					</div>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--logo -->
				<div >
					<a href="index.html">
						<ul>	
							<li><img src="{{asset('TPigeons/Client/logo.png')}}" height="60px" width="250px" alt="" /></li>
							<li><h1></h1></li>
							<div class="clearfix"> </div>
						</ul>
					</a>
				</div>
				<!--//logo-->
				<div class="header-right header-right-grid">
					<div class="profile_details_left"><!--notifications of menu start -->
						<ul class="nofitications-dropdown">
							
								
						
							</li>	
						</ul>
						<div class="clearfix"> </div>
					</div>
				</div>
				
				
				<div class="clearfix"> </div>
			</div>
			<!--search-box-->
					
				<!--//end-search-box-->
			<div class="header-right">
				
				<!--notification menu end -->
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img"><img src="images/a.png" alt=""> </span> 
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
								<li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li> 
								<li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->
		<!-- main content start-->
		



@yield('content')



		<!--footer-->
	
	<!-- Classie -->
		<script src="{{asset('TPigeons/admin/layoute/js/classie.js')}}"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- Bootstrap Core JavaScript --> 
		
        <script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/dev-loaders.js')}}"></script>
        <script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/dev-layout-default.js')}}"></script>
		<script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/jquery.marquee.js')}}"></script>
		<link href="{{asset('TPigeons/admin/layoute/css/bootstrap.min.css" rel="stylesheet')}}">
		
		<!-- candlestick -->
		<script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/jquery.jqcandlestick.min.js')}}"></script>
		<link rel="stylesheet" type="text/css" href="{{asset('TPigeons/admin/layoute/css/jqcandlestick.css')}}" />
		<!-- //candlestick -->
		
		<!--max-plugin-->
		<script type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/plugins.js')}}"></script>
		<!--//max-plugin-->
		
		<!--scrolling js-->
		<script src="{{asset('TPigeons/admin/layoute/js/jquery.nicescroll.js')}}"></script>
		<script src="{{asset('TPigeons/admin/layoute/js/scripts.js')}}"></script>
		<!--//scrolling js-->
		
		<!-- real-time-updates -->
	
		<!-- //real-time-updates -->
		<!-- error-graph -->
		<script language="javascript" type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/jquery.flot.errorbars.js')}}"></script>
		<script language="javascript" type="text/javascript" src="{{asset('TPigeons/admin/layoute/js/jquery.flot.navigate.js')}}"></script>
	
		<!-- //error-graph -->
		<!-- Skills-graph -->		
		<script src="{{asset('TPigeons/admin/layoute/js/Chart.Core.js')}}"></script>
		<script src="{{asset('TPigeons/admin/layoute/js/Chart.Doughnut.js')}}"></script>
		
		<!-- //Skills-graph -->
		<!-- status -->
		<script src="{{asset('TPigeons/admin/layoute/js/jquery.fn.gantt.js')}}"></script>
		
		<!-- //status -->
		
</body>
</html>