<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
@include('layoutesecode.imageProduit')
    <link rel="icon" type="image/png" href="{{asset('TPigeons/Client/images/icons/favicon.png')}}"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/fonts/themify/themify-icons.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/fonts/elegant-font/html-css/style.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/css-hamburgers/hamburgers.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/animsition/css/animsition.min.css')}}">
<!--===============================================================================================-->
<script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>

<?php if(!isset($_SESSION)){session_start();}
if(!isset($_SESSION['panier']['produits'])){$_SESSION['panier']['produits']=array();}
   if(!isset($_SESSION['panier']['pigeons'])){$_SESSION['panier']['pigeons']=array();} ?>



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
    .bs-example{
        margin: 20px;
    }
.bodyimg{background-image:url({{asset('PhotoPigeon/body.png')}});}

</style>




    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/daterangepicker/daterangepicker.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/slick/slick.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/vendor/lightbox2/css/lightbox.min.css')}}">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('TPigeons/Client/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body class="animsition">

    <!-- Header -->
      <header class="header1" >
        <!-- Header desktop -->
        <div class="container-menu-header">
            <div class="topbar">
                <div class="topbar-social">
                    <a href="#" class="topbar-social-item fa fa-facebook"></a>
                    <a href="#" class="topbar-social-item fa fa-instagram"></a>
                    <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                    <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                    <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                </div>

        @if ($errors->has('email')||$errors->has('password') )

<script type="text/javascript">
    swal({
  type: 'error',
  title: 'Erreur',
  text: ' {{ $errors->first('email') }}   {{ $errors->first('password') }}  ',

})
</script>
@endif


        @if (session()->get('email') )

<script type="text/javascript">

swal({
  type: 'error',
  title: 'Oops...',
  text: 'Email qui saiser est existe!',

})

</script>
@endif




        @if (session()->get('storecmd') )

<script type="text/javascript">

swal(
  'Bravo!',
  'Nous vous avons envoyé un lien par email pour confirmer! {{Auth::user()->email}}',
  'success'
)
</script>
@endif




       @if (session()->get('contact') )

<script type="text/javascript">

swal(
  'Votre Message est Bien Envouys!',
  '',
  'success'
)
</script>
@endif


        @if (session()->get('confirmercmd') )

<script type="text/javascript">

swal(
  'Bravo!',
  'Votre commande est Confirmer!',
  'success'
)
</script>
@endif



                <span class="topbar-child1">
                   
                </span>

                <div class="topbar-child2">


<!-- Modal --> 



@if(!Auth::user())
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal"> connexion/ inscription </button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">connexion/inscription </h4>
      </div>
      <div class="modal-body">
        <p>@include('auth.login')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>

  </div>
</div>

@else 


 <form  action="{{ route('logout') }}" method="POST" >
                                        @csrf

                                        <button type="submit" class="btn btn-info" >Déconnection</button>
                                    </form>
                                    @endif








 
                 
                </div>
            </div>

            <div class="wrap_header">
    <?php $admin=0;?>            <!-- Logo -->
                <a href="index.html" class="logo">
         <h3 style="font-family: Fixedsys;">  PIGEON.MA</h3> 
                </a>
 @if(Auth::user())

                <!-- Menu -->@if(Auth::user()->admin==1)
       
<?php $admin=1;?>
    <div class="wrap_menu">
                    <nav class="menu">
                        <ul class="main_menu">

                                 <li>
                                <a href="/admin">Acceuil</a>
                            </li>



                            <li>
                                <a href="/CmdPg">Commandes</a>
                            </li>

               

                                 <li>
                                     
                                <a class="header-icon1" alt="ICON" height="40px" >  
Gréer Annonces </a>
                                <ul class="sub_menu">
                                    <li><a href="/CreatePG">Pigeon</a></li>
                                    <li><a href="/CreatePR">Produit</a></li>
                                    <li><a href="/CreateBD">Bids</a></li>
                                     <li><a href="/categorie">Catégorie</a></li>
                                </ul></li>

                           


                             <li>
                                     
                                <a class="header-icon1" alt="ICON" height="40px" >  
Les Annonces Autoriser</a>
                                <ul class="sub_menu">
                                    <li><a href="/APG">Pigeon</a></li>
                                    <li><a href="/APR">Produit</a></li>
                                    <li><a href="/Abids">Bids</a></li>
                                </ul></li>




              <li>
                                     
                                <a class="header-icon1" alt="ICON" height="40px" >  
Les Annonces Non Autoriser
</a>
                             <ul class="sub_menu">
                                    <li><a href="/NPG">Pigeon</a></li>
                                    <li><a href="/NPR">Produit</a></li>
                                    <li><a href="/Nbids">Bids</a></li>
                                </ul></li>


                        </ul>
                    </nav>
                </div>

@endif @endif




@if($admin==0)



         <div class="wrap_menu">
                    <nav class="menu">
                        <ul class="main_menu">
                            <li>
                                <a href="/">Acceuil</a>
                            
                            </li>

                            <li>
                                <a href="/pigeons">Pigeons</a>
                            </li>

                            <li class="sale-noti">
                                <a href="/produits">Produits</a>
                            </li>

                            <li>
                                <a href="/bids">Bids</a>
                            </li>

                           


                            <li>
                                <a data-toggle="modal" data-target="#contact">Contact</a>
                            </li>

                             <li>    
 


  <input type="text" name="search" placeholder="Rechercher.." class="form-control" onkeyup="recherech()" style="background-color: rgba(255, 6, 0, 0.2); 
color: rgba(255, 6, 0, 0.2);" id="inputrech">

<div style="position: absolute;background-color:#D8D8D8 ; margin-top: 0%;" id="recherech" ></div>

    


</li>
                        </ul>
                    </nav>
                </div>


        



@endif



                <!-- Header Icon -->
                <div class="header-icons">
             <ul class="main_menu">
                <li>
                                           @if(Auth::user())
                                @if(Auth::user()->admin!=1)
                                <a >  <img src="{{asset('TPigeons/Client/login.png')}}" class="header-icon1" alt="ICON" height="40px">
{{Auth::user()->name}}</a>
                                <ul class="sub_menu">
                                  
                                         <li><a href="/MESPG">Mon Compte</a></li>
                                    <li><a href="/ModifierLogin">Paramétre</a></li>
                             


                                </ul>
                           

                                 @else



                                

    <a >   <img src="{{asset('PhotoPigeon/admin.png')}}" class="header-icon1" alt="ICON" height="40px">
{{Auth::user()->name}}</a>
                                <ul class="sub_menu">
                                    <li><a href="/admin">Page Admin</a></li>
                                  
 <a   href="/setting">
                            <li >paramètres
                            </li></a>
                                </ul>
                           

@endif @endif
                                  </li>
                        </ul>

                    


<?php if(!isset($_SESSION)){session_start();}
if(!isset($_SESSION['panier']['produits'])){$_SESSION['panier']['produits']=array();}
   if(!isset($_SESSION['panier']['pigeons'])){$_SESSION['panier']['pigeons']=array();} ?>






      <?php


foreach ($_SESSION['panier']['pigeons'] as $key => $id_pro) {
    $table[]=$id_pro;}
    if(!isset($table)){$table[]=0;}
 $panierPG =DB::table('pigeons')->whereIn('id_pig',$table)->get();
?>
                        <?php
foreach ($_SESSION['panier']['produits'] as $key => $id_pro) {
    $allid[]=$id_pro;}
      if(!isset($allid)){$allid[]=0;}
 $panierPR =DB::table('produits')->whereIn('id_pro',$allid)->get();
  ?>



                         
                  
                    <span class="linedivide1"></span>

      
                    <div class="header-wrapicon2">
                        <img src="{{asset('TPigeons/Client/images/icons/icon-header-02.png')}}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti" id="count"><?php echo $panierPG->count()+$panierPR->count();?></span>
<input type="hidden"  id="hidden" value="<?php echo $panierPG->count()+$panierPR->count();?>">
                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown" >
                    

                          <ul class="header-cart-wrapitem" id="panier">
                                @include('cleint.panier.panier')
 </ul>
                           

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="/Allpanier" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                     Voire  panier
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                   
                                </div>
                            </div>
                        </div>
                    </div>

<?php /* --------------------------------------------------------------*/?>

                </div>
            </div>
        </div>

        <!-- Header Mobile -->
        <div class="wrap_header_mobile">
            <!-- Logo moblie -->
            <a href="index.html" class="logo-mobile">
               PigeonMaroc.ma
            </a>

            <!-- Button show menu -->
            <div class="btn-show-menu">
                <!-- Header Icon mobile -->
                <div class="header-icons-mobile">
                    <a href="#" class="header-wrapicon1 dis-block">
                        <ul class="main_menu">
                <li>
                                           @if(Auth::user())
                                @if(Auth::user()->admin!=1)
                                <a >  <img src="{{asset('TPigeons/Client/login.png')}}" class="header-icon1" alt="ICON" height="40px">
{{Auth::user()->name}}</a>
                                <ul class="sub_menu">
                             
                                         <li><a href="/MESPG">Mon compte</a></li>
                                    <li><a href="/settingClient">paramètres</a></li>
                                </ul>
                           

                                 @else



                                

    <a >   <img src="{{asset('PhotoPigeon/admin.png')}}" class="header-icon1" alt="ICON" height="40px">
{{Auth::user()->name}}</a>
                                <ul class="sub_menu">
                                    <li><a href="/admin">Page Admin</a></li>
                                  
 <a   href="/setting">
                            <li >paramètres
                            </li></a>
                                </ul>
                           

@endif @endif
                                  </li>
                        </ul>
                    </a>

                    <span class="linedivide2"></span>

                  
                       

      
                    <div class="header-wrapicon2">
                        <img src="{{asset('TPigeons/Client/images/icons/icon-header-02.png')}}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <span class="header-icons-noti" id="count2"><?php echo $panierPG->count()+$panierPR->count();?></span>
<input type="hidden"  id="hidden" value="<?php echo $panierPG->count()+$panierPR->count();?>">
                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown" >
                    

                          <ul class="header-cart-wrapitem" id="panier2">
                                @include('cleint.panier.panier')
 </ul>
                           

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                    <a href="/Allpanier" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                     Voire  panier
                                    </a>
                                </div>

                                <div class="header-cart-wrapbtn">
                                    <!-- Button -->
                                   
                                </div>
                            </div>
                        </div>
                    </div>




                   
                    <?php /*------------------------------------------------------------*/?>
                </div>

                <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </div>
            </div>
        </div>

        <!-- Menu Mobile -->
        <div class="wrap-side-menu" >
            <nav class="side-menu">
                <ul class="main-menu">
                    <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                        <span class="topbar-child1">
                         
                        </span>
                    </li>
    @if(!Auth::user())
                <div class="size10 trans-0-4 m-t-10 m-b-10"  >
                    <!-- Button -->
                    
                    <a class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4" data-toggle="modal" data-target="#myModal2">
                    <font color="#FFFFF">Connecter</font> 
                    </a>
                </div>@else 

 <form  action="{{ route('logout') }}" method="POST" >
                                        @csrf

                                        <button type="submit" class="btn btn-danger" >Déconnection</button>
                                    </form>
                                    @endif






@if(Auth::user())  @if(Auth::user()->admin==1)
<div class="notificatio" style="margin-right:  5%;">

                      
<?php $nbrCs= DB::table('commandes')->select(DB::raw('count(*) as nbrC'))->where('deleted_at',null)->where('notification',0)->get();?>
<a href="/CmdPg">
<button class="btn btn-outline-secondary" type="button">
  Commandes <span class="badge">@foreach($nbrCs as $nbrC) {{$nbrC->nbrC}} @endforeach</span>
</button></a>





<?php $nbrPGs= DB::table('Pigeons')->select(DB::raw('count(*) as nbrPG'))->where('deleted_at',null)->where('notification',0)->where('bid',0)->where('status',0)->get();?>
<a href="/NPG">
<button class="btn btn-outline-secondary" type="button">
  Pigeon Non Attent <span class="badge">@foreach($nbrPGs as $nbrC) {{$nbrC->nbrPG}} @endforeach</span>
</button></a>




<?php $nbrPRs= DB::table('Produits')->select(DB::raw('count(*) as nbrPR'))->where('deleted_at',null)->where('notification',0)->where('status',0)->get();?>
<a href="/NPR">
<button class="btn btn-outline-secondary" type="button">
  Produits Non Attent <span class="badge">@foreach($nbrPRs as $nbrPR) {{$nbrPR->nbrPR}} @endforeach</span>
</button></a>



<?php $nbrBDs= DB::table('bids')->select(DB::raw('count(*) as nbrBD'))->where('deleted_at',null)->where('notification',0)->where('status',0)->get();?>
<a href="/Nbids">
<button class="btn btn-outline-secondary" type="button">
  Bids Non Attent <span class="badge">@foreach($nbrBDs as $nbrBD) {{$nbrBD->nbrBD}} @endforeach</span>
</button></a>







</div> 



 @endif @endif
















<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Connecter </h4>
      </div>
      <div class="modal-body">
        <p> @include('auth.logina') </p>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>






                    <li class="item-topbar-mobile p-l-10">
                        <div class="topbar-social-mobile">
                            <a href="#" class="topbar-social-item fa fa-facebook"></a>
                            <a href="#" class="topbar-social-item fa fa-instagram"></a>
                            <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                            <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                            <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                        </div>
                    </li>
@if(Auth::user()) @if(Auth::user()->admin==1)
                   <li class="item-menu-mobile">
                        <a href="/">Acceuil</a>
                     
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>

 <li class="item-menu-mobile">
                        <a href="/CmdPg">Les Commande</a>
                     
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>
  <li class="item-menu-mobile">
                        <a> ANNONCES Autoriser</a>
                        <ul class="sub-menu">
                            <li><a href="/APG">Pigeons</a></li>
                            <li><a href="/APG">
                                           Produits</a></li>
                            <li><a href="/Abids">Bids</a></li>
                        </ul>
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>


<li class="item-menu-mobile">
                        <a  class="arrow"> ANNONCES Non Autoriser</a>
                        <ul class="sub-menu">
                            <li><a href="/NPG">Pigeons</a></li>
                            <li><a href="/NPG">
                                           Produits</a></li>
                            <li><a href="/Nbids">Bids</a></li>
                        </ul>
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>


  <li class="item-menu-mobile">
                        <a href="/"> Poster une Annonces</a>
                        <ul class="sub-menu">
                            <li><a href="/CreatePG">Pigeons</a></li>
                            <li><a href="/CreatePR">
                                           Produits</a></li>
                            <li><a href="/CreateBD">Bids</a></li>
                             <li><a href="/categorie">Categorie</a></li>
                        </ul>
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>







                    <li class="item-menu-mobile">
                       
 <a href="/ModifierLogin">Paramétre</a>

                    </li>

      @else    
   <li class="item-menu-mobile">
                        <a href="/">Acceuil</a>
                     
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>

                    <li class="item-menu-mobile">
                        <a href="/pigeons">Pigeons</a>
                    </li>


                    <li class="item-menu-mobile">
                        <a href="/produits">Produits</a>
                    </li>
                 <li class="item-menu-mobile">
                        <a href="/bids">BIDS</a>
                    </li>

                    <li  class="item-menu-mobile">
                    <a href="/contact" data-toggle="modal" data-target="#contact">Contact</a>
                    </li>
@endif

        @else 
 <li class="item-menu-mobile">
                        <a href="/">Acceuil</a>
                     
                        <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                    </li>

                    <li class="item-menu-mobile">
                        <a href="/pigeons">Pigeons</a>
                    </li>


                    <li class="item-menu-mobile">
                        <a href="/produits">Produits</a>
                    </li>
                 <li class="item-menu-mobile">
                        <a href="/bids">BIDS</a>
                    </li>

                    <li  class="item-menu-mobile">
                    <a  data-toggle="modal" data-target="#contact">Contact</a>
                    </li>
                    @endif


                </ul>
            </nav>
        </div>
    </header >

















 





@yield('content')





<script type="text/javascript">








 function recherech() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("recherech").innerHTML = this.responseText;
    }
  };

nom=document.getElementById("inputrech").value;
if(nom.length==0){ document.getElementById("recherech").innerHTML=""; return;}





     xhttp.open("GET", "/recherech/"+nom, true);

  xhttp.send();





}























    function add_panierPG($id) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("panier").innerHTML = this.responseText;
          document.getElementById("panier2").innerHTML = this.responseText;
    }
  };




     xhttp.open("GET", "/add_panierPG/"+$id, true);

  xhttp.send();

swal({
  position: 'centre',
  type: 'success',
  title: 'Ajouter au Panier',
  showConfirmButton: false,
  timer: 1500
})

x=document.getElementById("hidden").value;
     document.getElementById("count").innerHTML=+x+1;
          document.getElementById("count2").innerHTML=+x+1;
document.getElementById("hidden").value=+x+1;


document.getElementById("btnPG"+$id).innerHTML= "";

}

    function add_panierPR($id) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("panier").innerHTML = this.responseText;
        document.getElementById("panier2").innerHTML = this.responseText;
    }
  };



     xhttp.open("GET", "/add_panierPR/"+$id, true);

  xhttp.send();

swal({
  position: 'centre',
  type: 'success',
  title: 'Ajouter au Panier',
  showConfirmButton: false,
  timer: 1500
})

 x=document.getElementById("hidden").value;
     document.getElementById("count").innerHTML=+x+1;
     document.getElementById("count2").innerHTML=+x+1;
document.getElementById("hidden").value=+x+1;
document.getElementById("btnPR"+$id).innerHTML= ""

}


    function removePR($id) {
  
swal({
  title: 'VOUS Etez sure ?',
  text: "!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {

    var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("panier").innerHTML = this.responseText;
         document.getElementById("panier2").innerHTML = this.responseText;
    }
  };



     xhttp.open("GET", "/remove_panierPR/"+$id, true);

  xhttp.send();

  x=document.getElementById("hidden").value;
     document.getElementById("count").innerHTML=+x-1;
       document.getElementById("count2").innerHTML=+x-1;
document.getElementById("hidden").value=+x-1;


    swal(
      'Deleted!',
      'Supprimer.',
      'success'
    )
  }
})



}





    function removePG($id) {


swal({
  title: 'VOUS Etez Sur?',
  text: "!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'OUI!'
}).then((result) => {

  if (result.value) {

      var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("panier").innerHTML = this.responseText;
        document.getElementById("panier2").innerHTML = this.responseText;
    }
  };




     xhttp.open("GET", "/remove_panierPG/"+$id, true);

  xhttp.send();

     x=document.getElementById("hidden").value;
     document.getElementById("count").innerHTML=+x-1;
      document.getElementById("count2").innerHTML=+x-1;
document.getElementById("hidden").value=+x-1;

    swal(
      'Opération terminée avec succès!',
    'Supprimer.',
      'success'
    )
  }
})


}








</script>

  <footer class="footer" style=" background-color: #D1D0CF">    
    <div class="footer__column-links">
      <div class="back-to-top"> <a href="#top" class="btn btn--round btn--round--lg"><span class="icon-arrow-up"></span></a></div>
      <div class="container">
        <div class="row">
          <div class="col-md-3 hidden-xs hidden-sm"> 
            <a class="logo logo--footer" href="index.html">   </a> 
            <dl class="footer-excerption">              
              <dd style="font-family: Broadway"> Pigeon maroc textPigeon maroc textPigeon maroc textPigeon maroc textPigeon maroc text </dd>
              <dt></dt>
            </dl>
          </div>
          <div class="col-sm-3 col-md-2 mobile-collapse">
            <h5 class="title text-uppercase mobile-collapse__title" style="font-family: Broadway">Information </h5>
            <div class="v-links-list mobile-collapse__content">
              <ul>
                <li style="font-family: Bahnschrift"><a href="#">à propos</a></li>
                <li style="font-family: Bahnschrift"><a href="#">Ventes & Retours</a></li>
                <li style="font-family: Bahnschrift"><a href="#">Condition</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-3 col-md-2 mobile-collapse">
            <h5 class="title text-uppercase mobile-collapse__title"  style="font-family: Broadway">Service</h5>
            <div class="v-links-list mobile-collapse__content">
              <ul>
                <li style="font-family: Bahnschrift"><a href="#">Support</a></li>
                <li style="font-family: Bahnschrift"><a href="#">FAQs</a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-3 col-md-2 mobile-collapse">
            <h5 class="title text-uppercase mobile-collapse__title" style="font-family: Broadway">Mon Compte</h5>
            <div class="v-links-list mobile-collapse__content">
              <ul>
                <li style="font-family: Bahnschrift"><a href="#">Mon Compte</a></li>
                <li style="font-family: Bahnschrift"><a href="#">Profile</a></li>
                <li style="font-family: Bahnschrift"><a href="#">Panier</a></li>
              </ul>
            </div>
          </div><?php $infos = DB::table('visiteur')->get();?>@foreach($infos as $info)
          <div class="col-sm-3 col-md-3 mobile-collapse mobile-collapse--last">
            <h5 class="title text-uppercase mobile-collapse__title" style="font-family: Broadway">Information Sté</h5>
            <div class="v-links-list mobile-collapse__content">
              <ul>
                <li class="icon icon-home" style="font-family: Bahnschrift">{{$info->Adresse}}</li>
                <li class="icon icon-telephone" style="font-family: Bahnschrift">{{$info->tel}}</li>
                <li class="icon icon-mail" style="font-family: Bahnschrift"><a href="mailto:info@mydomain.com">{{$info->Email}}</a></li>
              </ul>
            </div>@endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="footer__subscribe">
      <div class="container">
        <div class="row">
<br><br>
          <div class="col-md-6">
         
          </div>
        
          <div class="col-sm-12 col-md-12">
            <div class="social-links social-links--colorize social-links--large">
              <ul>
                <li class="social-links__item"><a class="icon icon-facebook" href="http://www.facebook.com/"></a></li>
                <li class="social-links__item"><a class="icon icon-twitter" href="http://www.twitter.com/"></a></li>
                <li class="social-links__item"><a class="icon icon-google" href="http://www.google.com/"></a></li>
                <li class="social-links__item"><a class="icon icon-linkedin" href="http://www.linkedin.com/"></a></li>
                <li class="social-links__item"><a class="icon icon-mail" href="mailto:mail@google.com"></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- <div class="footer__bottom">
      <div class="container">
        <div class="pull-left text-uppercase">©  2017 Hadaik Ain Asserdoune. Tous les droits sont réservés. </div>
        
        <div class="pull-right text-uppercase text-right">with love <span class="icon-favorite color-heart"></span> from <a href="http://themeforest.net/user/etheme">etheme</a></div>
      </div>
    </div> -->
  </footer>



  <!-- Back to top -->
  <div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
      <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
  </div>

  <!-- Container Selection1 -->
  <div id="dropDownSelect1"></div>



<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/bootstrap/js/popper.js')}}"></script>
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/select2/select2.min.js')}}"></script>
  <script type="text/javascript">
    $(".selection-1").select2({
      minimumResultsForSearch: 20,
      dropdownParent: $('#dropDownSelect1')
    });
  </script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/slick/slick.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('TPigeons/Client/js/slick-custom.js')}}"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
  <script type="text/javascript" src="{{asset('TPigeons/Client/vendor/lightbox2/js/lightbox.min.js')}}"></script>
<!--===============================================================================================-->

  <script type="text/javascript">
    $('.block2-btn-addcart').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to cart !", "success");
      });
    });

    $('.block2-btn-addwishlist').each(function(){
      var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
      $(this).on('click', function(){
        swal(nameProduct, "is added to wishlist !", "success");
      });
    });
  </script>

<!--===============================================================================================-->
  <script src="{{asset('TPigeons/Client/js/main.js')}}"></script>







<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="contact" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>@include('cleint.contact')</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>
