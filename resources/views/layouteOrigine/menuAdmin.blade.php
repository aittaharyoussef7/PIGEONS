<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/font-awesome/css/font-awesome.min.css')}}">
  <style type="text/css">
   table{border:2px solid black;border-style:outset;text-align:center;}
 table tr th{background-color:#A4A4A4;border:0.5px solid  darkgrey;color:floralwhite;text-align:center;}
 table tr td{font-size:15px;}

</style>


  <!-- Ionicons -->
<script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/dist/css/adminlte.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/daterangepicker/daterangepicker-bs3.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('TPigeons/admin/layoute/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

        @if ($errors->has('email')||$errors->has('password') )

<script type="text/javascript">
    swal({
  type: 'error',
  title: 'Erreur',
  text: ' {{ $errors->first('email') }}   {{ $errors->first('password') }}  ',

})
</script>
@endif
<body class="hold-transition sidebar-mini"  >
<div class="wrapper" >
  <navN id="colorN">
@include('ajax.colorN')
 </navN>
  <!-- /.navbar -->
<navM id="colorM">
@include('ajax.colorM')
</navM>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
        
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
          
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->












@yield('content')





















    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-alpha
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('TPigeons/admin/layoute/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->
<script src="{{asset('TPigeons/admin/layoute/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('TPigeons/admin/layoute/https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js')}}"></script>
<script src="{{asset('TPigeons/admin/layoute/plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('TPigeons/admin/layoute/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('TPigeons/admin/layoute/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('TPigeons/admin/layoute/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('TPigeons/admin/layoute/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js')}}"></script>
<script src="{{asset('TPigeons/admin/layoute/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('TPigeons/admin/layoute/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('TPigeons/admin/layoute/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('TPigeons/admin/layoute/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('TPigeons/admin/layoute/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('TPigeons/admin/layoute/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('TPigeons/admin/layoute/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('TPigeons/admin/layoute/dist/js/demo.js')}}"></script>
<script src="https://unpkg.com/sweetalert2@7.19.2/dist/sweetalert2.all.js"></script>
<script type="text/javascript">
  


 function recherech() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("recherech").innerHTML = this.responseText;
    }
  };

nom=document.getElementById("inputrech").value;
if(nom.length==0){ document.getElementById("recherech").innerHTML=""; return;}





     xhttp.open("GET", "/recherech/"+nom, true);

  xhttp.send();





}





 function colorM() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() { 
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("colorM").innerHTML = this.responseText;
    }
  };

colorMs=document.getElementById("inputM").value;

colorMs = colorMs.replace('#','');







     xhttp.open("GET", "/colorM/"+colorMs, true);

  xhttp.send();





}


 function colorN() {

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("colorN").innerHTML = this.responseText;
    }
  };

colorNs=document.getElementById("inputN").value;

colorNs = colorNs.replace('#','');







     xhttp.open("GET", "/colorN/"+colorNs, true);

  xhttp.send();





}


</script>


        @if (session()->get('email') )

<script type="text/javascript">


swal({
  type: 'error',
  title: 'Oops...',
  text: 'Email qui saiser est existe!',

})


</script>
@endif
</body>
</html>
